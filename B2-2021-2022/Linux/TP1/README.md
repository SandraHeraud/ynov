# TP1 : (re)Familiaration avec un système GNU/Linux

## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)


## 0. Préparation de la machine


🌞 **Setup de deux machines Rocky Linux configurée de façon basique.**

* un accès internet (via la carte NAT)
  * carte réseau dédiée

Pour afficher la carte réseau dédiée on effectue la commande ``` ip a ```
```
[kaly@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e7:6b:bd brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86286sec preferred_lft 86286sec
    inet6 fe80::a00:27ff:fee7:6bbd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```

  * route par défaut

La route est montrée par la commande ``` ip r s ``` ou ``` ip route show ```
```
[kaly@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
[...]
```
* un accès à un réseau local (les deux machines peuvent se `ping`) (via la carte Host-Only)
  * les machines doivent posséder une IP statique sur l'interface host-only
  * vous devez aussi choisir l'IP de votre hôte sur le réseau host-only 

Pour pouvoir configurer et choisir une IP statique, on modifie le fichier ```/etc/sysconfig/network-scripts/ifcfg-enp0s8```.

Pour se faire on utilise la commande ```sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8```

On modifie ensuite le fichier.
```
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
```
Pour finir, on redémarre l'interface avec la commande ```sudo nmcli con reload``` puis la commande ```sudo nmcli con up enp0s8```
```
[kaly@node1 ~]$ sudo nmcli con reload
[kaly@node1 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```

  * carte réseau dédiée (host-only sur VirtualBox)

On peut ensuite faire la commande ``` ip a ``` pour afficher la carte réseau dédiée en host-only.
```
[kaly@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e7:6b:bd brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85950sec preferred_lft 85950sec
    inet6 fe80::a00:27ff:fee7:6bbd/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2e:07:ea brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe2e:7ea/64 scope link
       valid_lft forever preferred_lft forever
```

* les machines doivent avoir un nom 
  * `/etc/hostname` 

Pour changer le nom de la machine, on utilise la commande ```sudo nano /etc/hostname```et on modifie ensuite le fichier.

```
node1.tp1.b2
```

On reboot ensuite la VM avec la commande ```reboot``` .

  * commande `hostname`

Après le reboot on peut utiliser la commande ``` hostname ``` pour afficher le nouveau nom.
```
[kaly@node1 ~]$ hostname
node1.tp1.b2
```

- **utiliser `1.1.1.1` comme serveur DNS**
  - vérifier avec le bon fonctionnement avec la commande `dig`
    - avec `dig`, demander une résolution du nom `ynov.com`

```
[kaly@node1 ~]$ [kaly@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57592
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               1794    IN      A       92.243.16.143

;; Query time: 22 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 18:13:10 CEST 2021
;; MSG SIZE  rcvd: 53
```

- mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé

```
;; ANSWER SECTION:
ynov.com.               1794    IN      A       92.243.16.143
```
- mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu

```
;; Query time: 22 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**
  - fichier `/etc/hosts`
```  
[kaly@node1 ~]$ sudo nano /etc/hosts


127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12 node2 node2.tp1.b2


```
  - assurez-vous du bon fonctionnement avec des `ping <NOM>`

```
[kaly@node1 ~]$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.589 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.545 ms
64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.363 ms
^C
--- node2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2047ms
rtt min/avg/max/mdev = 0.363/0.499/0.589/0.097 ms
```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
  - commande `firewall-cmd`

## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memo/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :

- le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
- le shell de l'utilisateur soit `/bin/bash`

```
[kaly@node1 ~]$ sudo useradd sandra -d /home -s /bin/bash -u 2000
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
```

🌞 Créer un nouveau groupe `admins` qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

```
[kaly@node1 ~]$ sudo groupadd admins
```

Pour permettre à ce groupe d'accéder aux droits `root` :

- il faut modifier le fichier `/etc/sudoers`
- on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
- la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
- ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)

```
[kaly@node1 ~]$ sudo visudo /etc/sudoers
```

🌞 Ajouter votre utilisateur à ce groupe `admins`.

```
[kaly@node1 ~]$ sudo usermod -aG admins sandra
[kaly@node1 ~]$ groups sandra
sandra : sandra admins
```
### 2. SSH

[Une section dédiée aux clés SSH existe dans le cours.](../../cours/ssh_keys/)

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine. 

🌞 Pour cela :

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )
  - génération de clé depuis VOTRE poste donc
  - sur Windows, on peut le faire avec le programme `puttygen.exe` qui est livré avec `putty.exe`
- déposer la clé dans le fichier `/home/<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer
  - vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP
  - on peut le faire à la main
  - ou avec la commande `ssh-copy-id`

  J' ai essayé la commande ```sudo nano /home/sandra/.ssh/authorized_keys``` mais impossible d'enregistrer le fichier : message d'erreur : le fichier n'existe pas 

🌞 Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.






