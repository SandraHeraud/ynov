# TP2 pt. 1 : Gestion de service

Dans ce TP on va s'orienter sur l'**utilisation des systèmes GNU/Linux** comme un outil pour **faire tourner des services**. C'est le principal travail de l'administrateur système : fournir des services.

Ces services, on fait toujours la même chose avec :

- **installation** (opération ponctuelle)
- **configuration** (opération ponctuelle)
- **maintien en condition opérationnelle** (opération continue, tant que le service est actif)

**Dans cette première partie, on va voir la partie installation et configuration.** Peu importe l'outil visé, de la base de données au serveur cache, en passant par le serveur web, le serveur mail, le serveur DNS, ou le serveur privé de ton meilleur jeu en ligne, c'est toujours pareil : install into conf.

La partie maintien en condition opérationnelle sera abordée dans la deuxième partie de ce TP.

**On va apprendre à maîtriser un peu ces étapes, et pas simplement suivre la doc.**

On va maîtriser le service fourni :

- manipulation du service avec systemd
- quel IP et quel port il utilise
- quels utilisateurs du système sont mobilisés
- quels processus sont actifs sur la machine pour que le service soit actif
- gestion des fichiers qui concernent le service et des permissions associées
- gestion avancée de la configuration du service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiqué par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

🌞 **Installer le serveur Apache**

- paquet `httpd`
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

```
[kaly@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
``` 

> Ce que j'entends au-dessus par "fichier de conf principal" c'est que c'est **LE SEUL** fichier de conf lu par Apache quand il démarre. C'est souvent comme ça : un service ne lit qu'un unique fichier de conf pour démarrer. Cherchez pas, on va toujours au plus simple. Un seul fichier, c'est simple.  
**En revanche** ce serait le bordel si on mettait toute la conf dans un seul fichier pour pas mal de services.  
Donc, le principe, c'est que ce "fichier de conf principal" définit généralement deux choses. D'une part la conf globale. D'autre part, il inclut d'autres fichiers de confs plus spécifiques.  
On a le meilleur des deux mondes : simplicité (un seul fichier lu au démarrage) et la propreté (éclater la conf dans plusieurs fichiers).

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
```
[kaly@web ~]$ sudo systemctl start httpd
```

  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
```
[kaly@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache

 ```
 [kaly@web ~]$ sudo ss -alnpt
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=823,fd=5))
LISTEN     0          128                     [::]:22                   [::]:*        users:(("sshd",pid=823,fd=7))
LISTEN     0          128                        *:80                      *:*        users:(("httpd",pid=1468,fd=4),("httpd",pid=1467,fd=4),("httpd",pid=1466,fd=4),("httpd",pid=1464,fd=4))
 ```   

 - [une petite portion du mémo est consacrée à `ss`](https://gitlab.com/it4lik/b2-linux-2021/-/blob/main/cours/memo/commandes.md#r%C3%A9seau)

**En cas de problème** (IN CASE OF FIIIIRE) vous pouvez check les logs d'Apache :

```bash
# Demander à systemd les logs relatifs au service httpd
$ sudo journalctl -xe -u httpd

# Consulter le fichier de logs d'erreur d'Apache
$ sudo cat /var/log/httpd/error_log

# Il existe aussi un fichier de log qui enregistre toutes les requêtes effectuées sur votre serveur
$ sudo cat /var/log/httpd/access_log
```

🌞 **TEST**

- vérifier que le service est démarré

```
[kaly@web ~]$ sudo systemctl is-active httpd
active
```
- vérifier qu'il est configuré pour démarrer automatiquement

```
[kaly@web ~]$ sudo systemctl is-enabled httpd
enabled
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[kaly@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
 [...] 
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```
User apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```
[kaly@web ~]$ ps -ef | grep httpd
root        1464       1  0 20:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1465    1464  0 20:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1466    1464  0 20:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      1467    1464  0 20:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      1468    1464  0 20:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
kaly        1924    1833  0 22:12 pts/0    00:00:00 grep --color=auto httpd
```

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf

```
[kaly@web ~]$ cd /usr/share/testpage/
[kaly@web testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Oct  7 19:50 .
drwxr-xr-x. 91 root root 4096 Oct  7 19:50 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```  

🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
```
[kaly@web testpage]$ sudo useradd nick -m -s /sbin/nologin
[kaly@web testpage]$ sudo cat /etc/passwd
[...]
nick:x:1001:1001::/home/nick:/sbin/nologin
```

- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
[kaly@web testpage]$ sudo nano /etc/httpd/conf/httpd.conf
[kaly@web testpage]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User nick
Group nick


ServerAdmin root@localhost

[...]
```

- redémarrez Apache
```
[kaly@web testpage]$ sudo systemctl restart httpd
```

- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
[kaly@web testpage]$ ps -ef | grep httpd
root        1965       1  0 22:27 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nick        1967    1965  0 22:27 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nick        1968    1965  0 22:27 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nick        1969    1965  0 22:27 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
nick        1970    1965  0 22:27 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
kaly        2184    1833  0 22:29 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix 
```
sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 7777

Include conf.modules.d/*.conf

User nick
Group nick
[...]
```

- ouvrez un nouveau port firewall, et fermez l'ancien
```
[kaly@web testpage]$ sudo firewall-cmd --add-port=7777/tcp --permanent
success
[kaly@web testpage]$ sudo firewall-cmd --reload
success
```

- redémarrez Apache
```
[kaly@web testpage]$ sudo systemctl restart httpd
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
[kaly@web testpage]$ sudo ss -antpl
State       Recv-Q      Send-Q           Local Address:Port             Peer Address:Port      Process
LISTEN      0           128                    0.0.0.0:22                    0.0.0.0:*          users:(("sshd",pid=823,fd=5))
LISTEN      0           128                       [::]:22                       [::]:*          users:(("sshd",pid=823,fd=7))
LISTEN      0           128                          *:7777                        *:*          users:(("httpd",pid=2711,fd=4),("httpd",pid=2710,fd=4),("httpd",pid=2709,fd=4),("httpd",pid=2706,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```
[kaly@web testpage]$ curl localhost:7777
<!doctype html>
<html>
  <head>
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

📁 **Fichier `/etc/httpd/conf/httpd.conf`** 

# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
- reprendre l'utilisateur par défaut
```
[kaly@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[sudo] password for kaly:
[kaly@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache

```

## 1. Intro

> Pas mal de blabla nécessaire avant de vous lancer. Lisez bien la partie en entier.

Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients. C'est sur son IP que les clients devront aller pour visiter le site web.  

La serveur de base de données `db.tp2.linux` sera un serveur uniquement accessible depuis `web.tp2.linux`. Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon j'ai un peu réfléchi et le but pour nous là c'est juste d'avoir un serv web + une db, peu importe ce que c'est le site. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment. Donc on va installer un truc un peu clé en main : Nextcloud.

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

Il y a [**une doc officielle Rocky Linux** plutôt bien fichue pour l'install de Nextcloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps), je vous laisse la suivre.


⚠️⚠️⚠️**ATTENTION** lisez bien toute la suite avant de vous lancer dans l'install, **lisez la partie en entier.** Ca vous évitera bien des soucis. ⚠️⚠️⚠️

Dans la doc officielle, on vous fait installer le serveur web (Apache) et la base de données (MariaDB) sur la même machine. **Ce n'est PAS ce que nous voulons : nous voulons avoir chaque service sur une machine dédiée.** Donc vous allez devoir ajuster la doc un petit peu. Rien de bien violent :

- quand on vous parle du serveur web, vous faites ça sur `web.tp2.linux`
- quand on vous parle de la base de données, vous faites ça sur `db.tp2.linux`
- à la fin, quand c'est en place, on vous demande d'aller l'interface Web de nextcloud et d'y saisir l'IP de la base de données, pour que NextCloud puisse s'y connecter. Vous saisirez ici l'IP de `db.tp2.linux` à la place de `localhost`

---
---

**Aussi** dans la doc vous est fourni un lien pour installer MariaDB de façon secure. C'est parfait, suivez-le.  
**Par contre** on ne vous donnez aucune infos sur quelle base de données créer dans le serveur de base de données.  
**Donc** avant de démarrer NextCloud, vous devrez :

- démarrer le service de base de données sur `db.tp2.linux`
- vous connecter au serveur de base de données
- exécutez des commandes SQL pour configurer la base, qu'elle soit utilisable par NextCloud

---
---

**Enfin, quelques tips en vrac pour que vous dérouliez l'install dans de bonnes conditions.** Certains tips vont vous paraître random. Beaucoup moins une fois que vous aurez déroulé la doc :

➜ ***Lisez*** et ***suivez bien toutes les instructions***. Toutes les étapes sont strictement nécessaires.

➜ Vous pouvez récupérer votre **timezone** plus facilement en une simple commande : `timedatectl`.

➜ **Si on vous parle d'un dossier et qu'il n'existe pas, créez-le.** Si vous ne savez quelles permissions lui donner, donnez-lui les mêmes permissions que les autres fichiers/dossiers qui se trouvent dans le même dossier.

➜ **Une fois que vous aurez fini d'installer NextCloud**, vous devez visiter l'interface Web. Vous ouvrirez donc votre navigateur, et vous rendrez à l'URL `http://web.tp2.linux`. La page d'accueil de NextCloud s'affichera. **NE VOUS CONNECTEZ PAS** et passez à l'installation de la base de données. C'est sur cet écran que vous indiquerez à NextCloud comment se connecter à votre base, une fois que vous l'aurez installé.

➜ Dans la doc, il est dit : "As noted earlier, we are using the ***"Apache Sites Enabled"*** procedure found here to configure Apache"

Cette ***"Apache Sites Enabled" procedure*** fait référence à une façon d'organiser le dossier `/etc/httpd` pour pas que ce soit le bordel :

- on a créé cette façon de faire car Apache est souvent utilisé pour héberger plusieurs sites web en même temps
- l'idée c'est de faire
  - un dossier `sites-available/` qui contient un fichier de configuration par site web
  - un dossier `sites-enabled/` qui contient des liens vers les fichiers de `sites-available`
- de cette façon il est plus simple de s'y retrouver :
  - un fichier par site, c'est clean
  - un dossier dédié à la conf des sites, et pas à la conf d'Apache plus générale, c'est clean
  - si on veut mettre un site hors-ligne sans faire de la crasse (commenter des lignes, supprimer la conf liée au site etc.) c'est EZ on a juste a supprimer le lien dans `sites-enabled/` ce qui garde intact le vrai fichier de conf dans `sites-available/`
- bref c'est clean quoi :)

**SAUF QUE** si vous suivez juste la doc, ça va pas fonctionner. En effet, les fichiers dans `sites-enabled/` ne seront jamais lus par défaut. Vous devez donc ajouter la ligne suivante dans le fichier `/etc/httpd/conf/httpd.conf` (tout en bas) :

```bash
IncludeOptional sites-enabled/*
```


## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

### A. Serveur Web et NextCloud

**Créez les 2 machines et déroulez la [📝**checklist**📝](#checklist).**

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

- n'oubliez pas de réinitialiser votre conf Apache avant de continuer
  - remettez le port et le user par défaut en particulier
- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps)
  - **uniquement pour le serveur Web + NextCloud**, vous ferez la base de données MariaDB après
  - quand ils parlent de la base de données, juste vous sautez l'étape, on le fait après :)
- je veux dans le rendu **toutes** les commandes réalisées
  - n'oubliez pas la commande `history` qui permet de voir toutes les commandes tapées précédemment

```
[kaly@web ~]$ dnf module list php
[...]
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                                         1.8 MB/s | 2.0 MB     00:01
Rocky Linux 8 - AppStream
Name                  Stream                    Profiles                                    Summary
php                   7.2 [d]                   common [d], devel, minimal                  PHP scripting language
php                   7.3                       common [d], devel, minimal                  PHP scripting language
php                   7.4                       common [d], devel, minimal                  PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                  Stream                    Profiles                                    Summary
php                   remi-7.2                  common [d], devel, minimal                  PHP scripting language
php                   remi-7.3                  common [d], devel, minimal                  PHP scripting language
php                   remi-7.4                  common [d], devel, minimal                  PHP scripting language
php                   remi-8.0                  common [d], devel, minimal                  PHP scripting language
php                   remi-8.1                  common [d], devel, minimal                  PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
[kaly@web ~]$ dnf module enable php:remi-7.4
[kaly@web ~]$ dnf module list php
Last metadata expiration check: 0:05:07 ago on Thu 07 Oct 2021 11:16:00 PM CEST.
Rocky Linux 8 - AppStream
Name                 Stream                       Profiles                                   Summary
php                  7.2 [d]                      common [d], devel, minimal                 PHP scripting language
php                  7.3                          common [d], devel, minimal                 PHP scripting language
php                  7.4                          common [d], devel, minimal                 PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                 Stream                       Profiles                                   Summary
php                  remi-7.2                     common [d], devel, minimal                 PHP scripting language
php                  remi-7.3                     common [d], devel, minimal                 PHP scripting language
php                  remi-7.4 [e]                 common [d], devel, minimal                 PHP scripting language
php                  remi-8.0                     common [d], devel, minimal                 PHP scripting language
php                  remi-8.1                     common [d], devel, minimal                 PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

[kaly@web ~]$ sudo mkdir /etc/httpd/sites-available
[kaly@web ~]$ sudo mkdir /etc/httpd/sites-enabled
[kaly@web ~]$ sudo vim /etc/httpd/sites-available/tp2.linux
[kaly@web ~]$ sudo cat /etc/httpd/sites-available/tp2.linux
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/com.yourdomain.nextcloud/html/
  ServerName  nextcloud.yourdomain.com

  <Directory /var/www/sub-domains/com.yourdomain.nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[kaly@web ~]$ sudo ln -s /etc/httpd/sites-available/tp2.linux /etc/httpd/sites-enabled/
[kaly@web ~]$ sudo mkdir -p /var/www/sub-domains/tp2.linux/html
[kaly@web zoneinfo]$ sudo vim /etc/opt/remi/php74/php.ini
[kaly@web zoneinfo]$ ls -al /etc/localtime
lrwxrwxrwx. 1 root root 34 Sep 15 15:31 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris
[kaly@web zoneinfo]$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[kaly@web zoneinfo]$ sudo unzip nextcloud-22.2.0.zip
[kaly@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/tp2.linux/html/
[kaly@web nextcloud]$ chown -Rf apache.apache /var/www/sub-domains/tp2.linux/html
[kaly@web nextcloud]$ sudo mkdir /var/www/sub-domains/tp2.linux/html/data
[kaly@web nextcloud]$ sudo mv /var/www/sub-domains/tp2.linux/html/data /var/www/sub-domains/tp2.linux/
[kaly@web nextcloud]$ sudo systemctl restart httpd
```
Une fois que vous avez la page d'accueil de NextCloud sous les yeux avec votre navigateur Web, **NE VOUS CONNECTEZ PAS** et continuez le TP

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)
📁 **Fichier `/etc/httpd/sites-available/web.tp2.linux`**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- manipulation 
- je veux dans le rendu **toutes** les commandes réalisées
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
  - exécutez les commandes SQL suivantes :

```sql
# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.linux
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

> Par défaut, vous avez le droit de vous connectez localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - vous pouvez utiliser la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données

> Les utilisateurs de la base de données sont différents des utilisateurs du système Linux sous-jacent. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

