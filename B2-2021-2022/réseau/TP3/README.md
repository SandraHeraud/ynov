# TP3 : Progressons vers le réseau d'infrastructure

Ce qu'on entend par "réseau" d'infrastructure c'est d'**adopter un point de vue admin** plutôt qu'un point de vue client.

Nous allons nous **positionner** de plus en plus comme les **personnes qui entretiennent le réseau** (admin), et non simplement comme les **personnes qui consomment le réseau** (client).

De quoi est composé, sommairement un réseau digne de ce nom :

- **une architecture réseau**
  - c'est l'organisation des switches, routeurs, firewall, etc, ainsi que le câblage entre eux
  - redondance des équipements, des logiciels, des solutions
  - on en verra que peu pour le moment
- **des services réseau d'infra**
  - des services très récurrents, nécessaire au fonctionnement normal d'un parc info
  - on parle ici, entre autres, de : DNS, DHCP, etc.
- **de services réseau orientés sur le maintien en condition opérationnelle**
  - accès aux machines via SSH
  - monitoring
  - sauvegarde
- **d'autres services, utiles directement aux clients du réseau**
  - serveur web
  - serveur de partage de fichiers

**BAH.** On est partis :D

![Here we gooo](./pic/sysadmin-hotline.gif "Here we gooo")

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [1. Adressage](#1-adressage)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# 0. Prérequis

➜ On reste sur full Rocky Linux côté machines virtuelles pour le tp : les clients, les routeurs, les serveurs. All Rocky meow. **N'hésitez pas à descendre à 1024Mo de RAM, voire moins. On a pas besoin de grand chose, et il va y avoir plusieurs machines dans ce TP.**

➜ [Référez-vous au mémo dédié pour les paquets à pré-installer dans le patron, ainsi que la conf à y effectuer.](../../cours/memo/install_vm.md)

➜ **Aucune carte NAT, à part sur la machine `router`** qui donnera un accès internet à tout le monde.

➜ On distinguera trois types de noeuds pendant le TP :

- *routeurs* :
  - IP statiques
  - routes statiques si besoin
- *serveurs* :
  - IP statiques
  - routes statiques si besoin
- *clients* :
  - IP dynamiques (récupérées en DHCP)
  - routes dynamiques (récupérées en DHCP)

➜ **Vous ne créerez aucune machine au début. Vous les créerez au fur et à mesure que le TP vous le demande.** A chaque fois qu'une nouvelle machine devra être créée, vous trouverez l'emoji 🖥️ avec son nom.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel
- [x] accès Internet (une route par défaut donc :) )
  - le routeur a une carte NAT
  - les autres machines passent par le routeur pour avoir internet (ajout de route par défaut)
- [x] accès à tous les LANs
  - route statiques ou dynamiques, suivant le type de noeud (voir un peu plus bas)
- [x] résolution de nom
  - vers internet
  - en local, grâce à votre propre serveur DNS (une fois qu'il sera monté, au début, ignorez cette étape)
- [x] modification des fichiers de zone DNS
  - attendez d'avoir passer l'étape du DNS pour faire ça à chaque fois
  - au début, ignorez cette étape

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. (mini)Architecture réseau

**Le but ici va être de mettre en place 3 réseaux, dont vous choisirez vous-mêmes les adresses.** Les contraintes :

- chaque réseau doit commencer par `10.3.`
  - `10` -> adresse privée
  - `3` -> c'est notre troisième TP :)
- chaque réseau doit être **le plus petit possible** : choisissez judicieusement vos masques
- aucun réseau ne doit se superposer (pas de doublons d'adresses IP possible)
- il y aura
  - deux réseaux pour des serveurs
  - un réseau pour des clients
- en terme de taille de réseau, vous compterez
  - 35 clients max pour le réseau `client1`
  - 63 serveurs max pour le réseau `server1`
  - 10 serveurs max pour le réseau `server2`
- les réseaux doivent, autant que possible, se suivre
- [référez-vous au cours dédié au subnetting](../../cours/ip/README.md#2-subnetting) si vous ne savez pas comment vous y prendre

> *Comme d'hab, pour le moment, chaque réseau = un host-only dans VirtualBox. Vous attribuerez comme IP à votre carte host-only (sur l'hôte, votre PC) **la première IP disponible du réseau.***

---

**Il y aura un routeur dans le TP**, qui acheminera les paquets d'un réseau à l'autre. Ce routeur sera donc la **passerelle** des 3 réseaux.  
On peut aussi dire qu'il sera la passerelle des machines, dans chacun des réseaux.  

> *Cette machine aura donc une carte réseau dans TOUS les réseaux.*

**Pour ce routeur, vous respecterez la convention qui consiste à lui attribuer la dernière IP disponible du réseau dans lequel il se trouve.**

> *Par exemple, dans le réseau `192.168.10.0/24`, par convention, on définira sur le routeur l'adresse IP `192.168.10.254`.*

## 1. Adressage

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |
| `server1`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |
| `server2`     | `10.3.?.?`        | `255.255.?.?` | ?                           | `10.3.?.?`         | `10.3.?.?`                                                                                   |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3` | `10.3.?.?/?`                                   | `10.3.?.?/?`           | `10.3.?.?/?`           | Carte NAT             |
| ...          | ...                                            | ...                    | ...                    | `10.3.?.?/?`          |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*
