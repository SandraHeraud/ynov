# TP2 : On va router des trucs

Au menu de ce TP, on va explorer les notions vu en cours un peu plus en profondeur, et vous allez les explorer par vous-mêmes :

- les échanges ARP et DHCP
- le routage
- notions de TCP et UDP

## Sommaire

- [TP2 : On va router des trucs](#tp2--on-va-router-des-trucs)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
  - [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
  - [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP](#1-mise-en-place-du-serveur-dhcp)
    - [2. Analyse de trames](#2-analyse-de-trames-2)
  - [Conclusion](#conclusion)

## 0. Prérequis

Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

Vous aurez besoin de deux réseaux host-only dans VirtualBox :

- un premier réseau `10.2.1.0/24`
- le second `10.2.2.0/24`
- **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

Quelques paquets seront souvent nécessaires dans les TPs, il peut être bon de les installer dans la VM que vous clonez :

- de quoi avoir les commandes :
  - `dig`
  - `tcpdump`
  - `nmap`
  - `nc`
  - `python3`
  - `vim` peut être une bonne idée

Vous devrez **systématiquement** utiliser SSH pour contrôler vos VMs.

Vos firewalls doivent **toujours** être actifs (et donc correctement configurés).

Aussi, **autre rappel**, vos machines doivent **toujours être nommées** ([définition du hostname](../../cours/memo/rocky_network.md#changer-son-nom-de-domaine)).

Dernière chose : **pour toutes les parties avec Wireshark**, vous devrez systématiquement fournir le fichier `.pcap` dans le rendu de TP. Pour le compte-rendu, vous pouvez screen Wireshark si besoin. Un petit emoji 📁 sera présent à chaque fois qu'une capture devra être dans le rendu.

## I. ARP

Première partie simple, on va avoir besoin de 2 VMs.

| Machine          | NAT ? | `10.2.1.0/24` |
|------------------|-------|---------------|
| `node1.net1.tp2` | non   | `10.2.1.11`   |
| `node2.net1.tp2` | non   | `10.2.1.12`   |

```schema
   node1               node2
  ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     │
  └─────┘    └───┘    └─────┘
```

> Référez-vous au [mémo Réseau Rocky](../../cours/memo/rocky_network.md) pour connaître les commandes nécessaire à la réalisation de cette partie.

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre

```
[kaly@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.590 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.336 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.382 ms
[...]
^C
--- 10.2.1.12 ping statistics ---
13 packets transmitted, 13 received, 0% packet loss, time 12293ms
rtt min/avg/max/mdev = 0.297/0.352/0.590/0.073 ms
```

```
[kaly@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.371 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.370 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.317 ms
[...]
^C
--- 10.2.1.11 ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6140ms
rtt min/avg/max/mdev = 0.313/0.336/0.371/0.024 ms
```
- observer les tables ARP des deux machines

```
[kaly@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:3c DELAY
10.2.1.12 dev enp0s8 lladdr 08:00:27:9c:f1:33 STALE
```

```
[kaly@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:68:78:08 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:3c DELAY
```
- repérer l'adresse MAC de `node1` dans la table ARP de `node2` et vice-versa

| Machine          | Adresse Mac            |
|------------------|------------------------|
| `node1`          |` 08:00:27:68:78:08`    |
| `node2`          |` 08:00:27:9c:f1:33`    |


- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la table ARP de `node1`
  ```
  [kaly@node1 ~]$ ip n s
  10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:3c DELAY
  10.2.1.12 dev enp0s8 lladdr 08:00:27:9c:f1:33 STALE
  ```
  - et une commande pour voir la MAC de `node2`
  ```
  [kaly@node2 ~]$ ip a
  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
  2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:f1:33 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe9c:f133/64 scope link
       valid_lft forever preferred_lft forever
  ```

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame

```
[kaly@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22
[sudo] password for kaly:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C12 packets captured
12 packets received by filter
0 packets dropped by kernel
```

- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`

```
[kaly@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.543 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.333 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.400 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.368 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3091ms
rtt min/avg/max/mdev = 0.333/0.411/0.543/0.079 ms
```

- stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
- mettez en évidence les trames ARP

![wireshark-arp](./image/tp2-arp.png)

- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames**

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame   | source                      | destination                 |
|-------|--------------|-----------------------------|-----------------------------|
| 1     | Requête ARP  | `node1` `08:00:27:9c:f1:33` | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP  | `node2` `08:00:27:68:78:08` | `node1` `08:00:27:9c:f1:33` |
| 3     | Requête ICMP | `node1` `08:00:27:9c:f1:33` | `node2` `08:00:27:68:78:08` |
| 4     | Requête ICMP | `node2` `08:00:27:68:78:08` | `node1` `08:00:27:9c:f1:33` |
| ...   | ...          | ...                         | ...                         |
| 5     | Requête ARP  | `node2` `08:00:27:68:78:08` | `node1` `08:00:27:9c:f1:33` |
| 6     | Requête ARP  | `node1` `08:00:27:9c:f1:33` | `node2` `08:00:27:68:78:08` |

📁 Capture `tp2_arp.pcap`


> Si vous ne savez pas comment récupérer votre fichier `.pcap` sur votre hôte afin de l'ouvrir dans Wireshark, demandez-moi.

## II. Routage

Vous aurez besoin de 3 VMs pour cette partie.

| Machine           | NAT ? | `10.2.1.0/24` | `10.2.2.0/24` |
|-------------------|-------|---------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.254`  | `10.2.2.254`  |
| `node1.net1.tp2`  | no    | `10.2.1.11`   | no            |
| `marcel.net2.tp2` | no    | no            | `10.2.2.12`   |

> Je l'ai appelé `marcel` histoire de voir un peu plus clair dans les noms 🌻

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └───┘    └─────┘    └───┘    └─────┘
```

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router.net2.tp2`**

> Cette étape est nécessaire car Rocky Linux c'est pas un OS dédié au routage par défaut. Ce n'est bien évidemment une opération qui n'est pas nécessaire sur un équipement routeur dédié comme du matériel Cisco.

A faire sur le routeur :
```
[kaly@routeur ~]$ sudo firewall-cmd --list-all
[sudo] password for kaly:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kaly@routeur ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[kaly@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[kaly@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```


🌞**Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se `ping`**

- il faut ajouter une seule route des deux côtés

A faire sur les deux machine :
```
[kaly@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/route-enp0s8

---------------------------------------------------------------------------

10.2.1.11/24 via 10.2.2.254 dev marcel

---------------------------------------------------------------------------

[kaly@marcel ~]$ sudo nmcli con reload
[sudo] password for kaly:
[kaly@marcel ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)

```

- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre

```
[kaly@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.09 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.527 ms
^C
--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.527/0.808/1.090/0.282 ms

---------------------------------------------------------------------------

[kaly@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.37 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.638 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.638/1.005/1.372/0.367 ms


```


### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
- effectuez un `ping` de `node1.net1.tp2` vers `marcel.net2.tp2`
- regardez les tables ARP des trois noeuds
- essayez de déduire un peu les échanges ARP qui ont eu lieu
- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `node1` et `marcel`, afin de capturer les échanges depuis les 2 points de vue
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | IP source  | MAC source                    | IP destination | MAC destination               |
|-------|-------------|------------|-------------------------------|----------------|-------------------------------|
| 1     | Requête ARP | 10.2.1.11  | `node1` `08:00:27:68:78:08`   | 10.2.1.254     | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | Ping ICMP   | 10.2.2.254 | `routeur` `08:00:27:c7:19:6f` | 10.2.2.12      | `marcel` `08:00:27:5f:90:8b`  |
| 3     | Requête ARP | 10.2.2.12  | `marcel` `08:00:27:5f:90:8b`  | 10.2.2.254     | Broadcast `FF:FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | 10.2.1.254 | `routeur` `08:00:27:c5:a2:51` | 10.2.1.11      | `node1` `08:00:27:68:78:08`   |
| 5     | Ping ICMP   | 10.2.1.11  | `node1` `08:00:27:68:78:08`   | 10.2.2.12      | `routeur` `08:00:27:c5:a2:91` |
| 6     | Réponse ARP | 10.2.2.254 | `routeur` `08:00:27:c7:19:6f` | 10.2.2.12      | `marcel` `08:00:27:5f:90:8b`  |
| 7     | Pong ICMP   | 10.2.2.12  | `marcel` `08:00:27:5f:90:8b`  | 10.2.2.254     | `routeur` `08:00:27:c7:19:6f` |
| 8     | Pong ICMP   | 10.2.2.12  | `routeur` `08:00:27:c5:a2:91` | 10.2.1.11      | `node1` `08:00:27:68:78:08`   |
| 9     | Ping ICMP   | 10.2.1.11  | `node1` `08:00:27:68:78:08`   | 10.2.2.12      | `routeur` `08:00:27:c5:a2:91` |
| 10    | Ping ICMP   | 10.2.2.254 | `routeur` `08:00:27:c7:19:6f` | 10.2.2.12      | `marcel` `08:00:27:5f:90:8b`  |
| 11    | Pong ICMP   | 10.2.2.12  | `marcel` `08:00:27:5f:90:8b`  | 10.2.2.254     | `routeur` `08:00:27:c7:19:6f` |
| 12    | Pong ICMP   | 10.2.2.12  | `routeur` `08:00:27:c5:a2:91` | 10.2.1.11      | `node1` `08:00:27:68:78:08`   |
| 13    | Requête ARP | 10.2.1.254 | `routeur` `08:00:27:c5:a2:91` | 10.2.1.11      | `node1` `08:00:27:68:78:08`   |
| 14    | Requête ARP | 10.2.1.11  | `node1` `08:00:27:68:78:08`   | 10.2.1.254     | `routeur` `08:00:27:c5:a2:91` |


📁 Capture réseau `tp2_routage_node1.pcap` `tp2_routage_marcel.pcap`

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- le routeur a déjà un accès internet
- ajoutez une route par défaut à `node1.net1.tp2` et `marcel.net2.tp2`
```
[kaly@node1 ~]$ sudo nano /etc/sysconfig/network
[sudo] password for kaly:
[kaly@node1 ~]$ [kaly@node1 ~]$ sudo cat /etc/sysconfig/network
# Created by anaconda
GATEWAY=10.2.1.254
[kaly@node1 ~]$ sudo nmcli con reload
[kaly@node1 ~]$ sudo nmcli con up enp0s8
```
  - vérifiez que vous avez accès internet avec un `ping`
  - le `ping` doit être vers une IP, PAS un nom de domaine
```
[kaly@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=21.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=54.5 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 21.043/37.757/54.472/16.715 ms
```
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
```
[kaly@node1 ~]$ sudo nano /etc/resolv.conf
[kaly@node1 ~]$ [kaly@node1 ~]$ curl gitlab.com
[kaly@node1 ~]$ dig gitlab.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9820
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             287     IN      A       172.65.251.78

;; Query time: 5 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Wed Sep 29 16:52:46 CEST 2021
;; MSG SIZE  rcvd: 55

[kaly@node1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search auvence.co net1.tp2
nameserver 10.33.10.2
nameserver 10.33.10.148
nameserver 10.33.10.155
nameserver 1.1.1.1
```
  - puis avec un `ping` vers un nom de domaine
```
[kaly@node1 ~]$ ping google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=113 time=69.0 ms
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=2 ttl=113 time=20.0 ms
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=3 ttl=113 time=21.7 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 20.007/36.895/69.021/22.727 ms
```

🌞Analyse de trames

- effectuez un `ping 8.8.8.8` depuis `node1.net1.tp2`
- capturez le ping depuis `node1.net1.tp2` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source           | MAC source                    | IP destination      | MAC destination                |
|-------|------------|---------------------|-------------------------------|---------------------|--------------------------------|
| 1     | ping       | `node1` `10.2.1.11` | `node1` `08:00:27:68:78:08`   | `8.8.8.8`           | `routeur` `08:00:27:c5:a2:91`  |
| 2     | pong       | `8.8.8.8`           | `routeur` `08:00:27:c5:a2:91` | `node1` `10.2.1.11` | `node1` `08:00:27:68:78:08`    |

📁 Capture réseau `tp2_routage_internet.pcap`

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine           | NAT ? | `10.2.1.0/24`              | `10.2.2.0/24` |
|-------------------|-------|----------------------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.254`                | `10.2.2.254`   |
| `node1.net1.tp2`  | no    | `10.2.1.11`                | no            |
| `node2.net1.tp2`  | no    | oui mais pas d'IP statique | no            |
| `marcel.net2.tp2` | no    | no                         | `10.2.2.12`   |

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   node2       │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `node1.net1.tp2`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `node1.net1.tp2`
- créer une machine `node2.net1.tp2`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur
```
[kaly@node2 ~]$ ip a
[...]
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:f1:33 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 455sec preferred_lft 455sec
    inet6 fe80::a00:27ff:fe9c:f133/64 scope link
       valid_lft forever preferred_lft forever
```
> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).


🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `node2.net1.tp2` pour tester :
  - `node2.net1.tp2` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP
    - vérifier qu'il peut `ping` sa passerelle
```
    [kaly@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:f1:33 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 300sec preferred_lft 300sec
    inet6 fe80::a00:27ff:fe9c:f133/64 scope link
       valid_lft forever preferred_lft forever
[kaly@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.549 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.319 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1026ms
rtt min/avg/max/mdev = 0.319/0.434/0.549/0.115 ms
```
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
    - vérifier que la route fonctionne avec un `ping` vers une IP
```
[kaly@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.2 metric 100
[kaly@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=19.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=22.9 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 19.430/23.139/27.089/3.131 ms
```
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne

```
[kaly@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56025
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             128     IN      A       142.250.178.142

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 16:39:47 CEST 2021
;; MSG SIZE  rcvd: 55
```
  - vérifier un `ping` vers un nom de domaine
```
[kaly@node2 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=113 time=23.6 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=113 time=21.2 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 21.228/22.395/23.562/1.167 ms
```

### 2. Analyse de trames

🌞**Analyse de trames**

- videz les tables ARP des machines concernées
- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
  - choisissez vous-mêmes l'interface où lancer la capture
- répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcap`
- mettez en évidence l'échange DHCP *DORA* (Disover, Offer, Request, Acknowledge)
- **écrivez, dans l'ordre, les échanges ARP + DHCP qui ont lieu, je veux TOUTES les trames** utiles pour l'échange 

📁 Capture réseau `tp2_dhcp.pcap`



