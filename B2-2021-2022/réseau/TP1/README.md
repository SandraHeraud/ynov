
# TP1 : Mise en jambes

## Sommaire
- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [En ligne de commande](#en-ligne-de-commande)
    - [En graphique (GUI : Graphical User Interface)](#en-graphique-gui--graphical-user-interface)
    - [Questions](#questions)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
    - [B. Table ARP](#b-table-arp)
    - [C. `nmap`](#c-nmap)
    - [D. Modification d'adresse IP (part 2)](#d-modification-dadresse-ip-part-2)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [1. Prérequis](#1-prérequis)
  - [2. Câblage](#2-câblage)
  - [Création du réseau (oupa)](#création-du-réseau-oupa)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)
- [Bilan](#bilan)


# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### Affichez les infos des cartes réseau de votre PC

### En ligne de commande


Avec la commande : `` ipconfig /all ``

nom, adresse MAC et adresse IP de l'interface WiFi

```
[...]
Carte réseau sans fil Wi-Fi :

Adresse physique . . . . . . . . . . . : 70-66-55-CF-4B-3B
Adresse IPv6 de liaison locale. . . . .: fe80::bdd5:65ba:fe7a:abf5%20(préféré)
Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.203(préféré)
[...]
```
nom, adresse MAC et adresse IP de l'interface Ethernet

```
[...]
Carte Ethernet Ethernet :

Statut du média. . . . . . . . . . . . : Média déconnecté
Adresse physique . . . . . . . . . . . : D4-5D-64-61-2D-19
[...]
```
### Affichez votre gateway

Avec la commande : `` ipconfig /all ``

```
[...]
Carte réseau sans fil Wi-Fi :

Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]
```

### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

- trouvez l'IP, la MAC et la [gateway](../../cours/lexique.md#passerelle-ou-gateway) pour l'interface WiFi de votre PC

(depuis mon réseau personnel)

![GUI-adresse-IP](./image/GUI-adresse-IP.png)


### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

La gateway est une adresse ip qui nous permet de sortir du réseau d'ynov.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre
- ne changez que le dernier octet
  - par exemple pour `10.33.1.10`, echangez que le "10"
  - valeur entre 1 et 254 compris

![GUI-modification](./image/GUI-modification.png)

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

En modifiant son adresse IP manuellement on prend le risque de se retrouver avec une adresse IP déjà attribuée et donc de perdre sa connexion avec internet.

---

- **NOTE :** si vous utilisez la même IP que quelqu'un d'autre, il se passerait la même chose qu'en vrai avec des adresses postales :
  - deux personnes habitent au même numéro dans la même rue, mais dans deux maisons différentes
  - quand une de ces personnes envoie un message, aucun problème, l'adresse du destinataire est unique, la lettre sera reçue
  - par contre, pour envoyer un message à l'une de ces deux personnes, le facteur sera dans l'impossibilité de savoir dans quelle boîte aux lettres il doit poser le message
  - ça marche à l'aller, mais pas au retour

### B. Table ARP

La table ARP c'est votre "table de voisinnage". Elle contient la liste des adresses MAC des machines avec qui vous avez communiqué récemment.

Quand deux machines communiquent, elles enregistrent mutuellement l'adresse MAC et l'adresse IP de l'autre dans cette table.

> L'échange d'adresse MAC se fait de façon automatique, dès qu'on essaie de contacter une machine via son IP, et se fait à l'aide du protocole ARP, que nous verrons plus tard.

🌞 Exploration de la table ARP

- depuis la ligne de commande, afficher la table ARP
- identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

```
C:\Users\sandr>arp -a

Interface : 10.10.1.1 --- 0x9
  Adresse Internet      Adresse physique      Type
  10.10.1.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.229.1 --- 0xe
  Adresse Internet      Adresse physique      Type
  192.168.229.254       00-50-56-fc-85-41     dynamique
  192.168.229.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.30.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.30.254        00-50-56-fa-43-19     dynamique
  192.168.30.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.1.86 --- 0x12
  Adresse Internet      Adresse physique      Type
  192.168.1.1           cc-2d-1b-6c-38-d0     dynamique <-
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

```
L'adresse MAC correspondant à l'adresse IP de la passerelle est ``` cc-2d-1b-6c-38-d0 ```.

🌞 Et si on remplissait un peu la table ?

- envoyez des ping vers des IP du même réseau que vous. Lesquelles ? menfou, random. Envoyez des ping vers au moins 3-4 machines.
- affichez votre table ARP
- listez les adresses MAC associées aux adresses IP que vous avez ping

Etant sur le réseau de mon domicile, j'ai cherché à ping les adresses IP des téléphones de ma famille.

```

Interface : 192.168.1.86 --- 0x12
  Adresse Internet      Adresse physique      Type
  192.168.1.1           cc-2d-1b-6c-38-d0     dynamique
  192.168.1.32          24-18-1d-31-e6-98     dynamique <-
  192.168.1.43          82-20-5d-26-60-58     dynamique <-
  192.168.1.55          dc-ef-ca-54-82-62     dynamique <-
  192.168.1.97          46-0c-d1-af-fd-5d     dynamique <-
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

```
---

Ping des IP pour savoir si elles sont disponibles, c'est possible, mais c'est chiant.  
Ca serait bien un outil pour scanner le réseau à un instant T afin de choisir une adresse IP libre, non ?


### C. `nmap`

`nmap` est un outil de scan réseau. On peut faire des tas de choses avec, on va se cantonner à des choses basiques pour le moment.  
Les commandes `nmap` se présentent comme : `nmap OPTIONS CIBLE`

- `nmap` est le nom de la commande
- les `OPTIONS` se précisent avec le caractère `-` comme pour beaucoup de commandes
  - exemple : `nmap -sP` (`sP` c'est un Ping Scan, on y reviendra)
- la cible est soit une adresse de réseau (on cible tous les hôtes du réseau), soit un hôte simple 
  - hôte simple : `nmap -sP 10.33.1.53`
  - [réseau](../../cours/lexique.md#adresse-de-r%C3%A9seau) : `nmap -sP 10.33.0.0/22` (notation *CIDR*)

---

Téléchargez `nmap` depuis le [site officiel](https://nmap.org/download.html) pour votre OS

---

- exemple de commande pour le réseau YNOV  `nmap -sn -PE <ADRESSE_DU_RESEAU_CIBLE>` pour trouver les hôtes actuellement sur le réseau
  - le réseau YNOV c'est `10.33.0.0/22`
- scan de `ping` avec `nmap -sP`
  - par exemple `nmap -sP 10.33.0.0/22` pour un ping scan du réseau YNOV
- `nmap` est un outil de scan de réseau très puissant, on en reparlera !

---

``` 
PS C:\Users\sandr\Downloads\nmap-7.92-win32\nmap-7.92> nmap -sP 192.168.1.0/24
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-19 21:31 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 192.168.1.1
Host is up (0.0044s latency).
MAC Address: CC:2D:1B:6C:38:D0 (SFR)
Nmap scan report for 192.168.1.32
Host is up (0.20s latency).
MAC Address: 24:18:1D:31:E6:98 (Samsung Electro-mechanics(thailand))
Nmap scan report for 192.168.1.37
Host is up (0.13s latency).
MAC Address: B0:E8:92:C5:CD:AC (Seiko Epson)
Nmap scan report for 192.168.1.43
Host is up (0.097s latency).
MAC Address: 82:20:5D:26:60:58 (Unknown)
Nmap scan report for 192.168.1.46
Host is up (0.021s latency).
MAC Address: 60:35:C0:F6:44:EC (SFR)
Nmap scan report for 192.168.1.55
Host is up (0.40s latency).
MAC Address: DC:EF:CA:54:82:62 (Murata Manufacturing)
Nmap scan report for 192.168.1.61
Host is up (0.35s latency).
MAC Address: C0:E4:34:3D:12:59 (AzureWave Technology)
Nmap scan report for 192.168.1.97
Host is up (0.35s latency).
MAC Address: 46:0C:D1:AF:FD:5D (Unknown)
Nmap scan report for 192.168.1.86
Host is up.
Nmap done: 256 IP addresses (9 hosts up) scanned in 15.14 seconds
```

Suivant ce que vous faites avec `nmap` il y a moyen que ce soit bien le foutoir sur le réseau YNOV, un peu plus tard dans le TP il y aura une partie pour analyser ce qui passe sur le réseau.

Alors mollo.

🌞**Utilisez `nmap` pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre**

- réinitialiser votre conf réseau (reprenez une IP automatique, en vous déconnectant/reconnectant au réseau par exemple)
- lancez **un scan de ping** sur le réseau YNOV
- affichez votre table ARP

```
PS C:\Users\sandr\Downloads\nmap-7.92-win32\nmap-7.92> nmap -sP 192.168.1.0/24
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-19 21:36 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for box (192.168.1.1)
Host is up (0.0020s latency).
MAC Address: CC:2D:1B:6C:38:D0 (SFR)
Nmap scan report for 192.168.1.32
Host is up (0.10s latency).
MAC Address: 24:18:1D:31:E6:98 (Samsung Electro-mechanics(thailand))
Nmap scan report for EPSONC5CDAC (192.168.1.37)
Host is up (0.10s latency).
MAC Address: B0:E8:92:C5:CD:AC (Seiko Epson)
Nmap scan report for iPad-de-Sandra (192.168.1.43)
Host is up (0.0090s latency).
MAC Address: 82:20:5D:26:60:58 (Unknown)
Nmap scan report for 192.168.1.46
Host is up (0.0050s latency).
MAC Address: 60:35:C0:F6:44:EC (SFR)
Nmap scan report for Galaxy-S8 (192.168.1.55)
Host is up (0.32s latency).
MAC Address: DC:EF:CA:54:82:62 (Murata Manufacturing)
Nmap scan report for Roomba-3198860410421870 (192.168.1.61)
Host is up (0.29s latency).
MAC Address: C0:E4:34:3D:12:59 (AzureWave Technology)
Nmap scan report for 192.168.1.97
Host is up (0.16s latency).
MAC Address: 46:0C:D1:AF:FD:5D (Unknown)
Nmap scan report for DESKTOP-7C6EJL1 (192.168.1.85)
Host is up.
Nmap done: 256 IP addresses (9 hosts up) scanned in 6.76 seconds

PS C:\Users\sandr\Downloads\nmap-7.92-win32\nmap-7.92> arp -a

Interface : 10.10.1.1 --- 0x9
  Adresse Internet      Adresse physique      Type
  10.10.1.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.229.1 --- 0xe
  Adresse Internet      Adresse physique      Type
  192.168.229.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.30.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.30.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.1.85 --- 0x12
  Adresse Internet      Adresse physique      Type
  192.168.1.1           cc-2d-1b-6c-38-d0     dynamique
  192.168.1.37          b0-e8-92-c5-cd-ac     dynamique
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
### D. Modification d'adresse IP (part 2)

- 🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à `nmap`
  - utilisez un ping scan sur le réseau YNOV
  - montrez moi la commande `nmap` et son résultat
  - configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)
  - prouvez en une suite de commande que vous avez bien l'IP choisie, que votre passerelle est bien définie, et que vous avez un accès internet

> Pour tester l'accès à internet, on ping souvent des addresses IP connues de serveurs sur internet. Comme le server DNS de CloudFlare `1.1.1.1` ou celui de Google `8.8.8.8`.

![GUI-modification2](./image/GUI-modification2.png)

```
PS C:\Users\sandr\Downloads\nmap-7.92-win32\nmap-7.92> nmap -sP 192.168.1.0/24
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-19 21:42 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 192.168.1.1
Host is up (0.019s latency).
MAC Address: CC:2D:1B:6C:38:D0 (SFR)
Nmap scan report for 192.168.1.32
Host is up (0.36s latency).
MAC Address: 24:18:1D:31:E6:98 (Samsung Electro-mechanics(thailand))
Nmap scan report for 192.168.1.37
Host is up (0.22s latency).
MAC Address: B0:E8:92:C5:CD:AC (Seiko Epson)
Nmap scan report for 192.168.1.43
Host is up (0.0090s latency).
MAC Address: 82:20:5D:26:60:58 (Unknown)
Nmap scan report for 192.168.1.46
Host is up (0.0060s latency).
MAC Address: 60:35:C0:F6:44:EC (SFR)
Nmap scan report for 192.168.1.55
Host is up (0.22s latency).
MAC Address: DC:EF:CA:54:82:62 (Murata Manufacturing)
Nmap scan report for 192.168.1.61
Host is up (0.21s latency).
MAC Address: C0:E4:34:3D:12:59 (AzureWave Technology)
Nmap scan report for 192.168.1.97
Host is up (0.16s latency).
MAC Address: 46:0C:D1:AF:FD:5D (Unknown)
Nmap scan report for 192.168.1.24
Host is up.
Nmap done: 256 IP addresses (9 hosts up) scanned in 10.00 seconds

PS C:\Users\sandr\Downloads\nmap-7.92-win32\nmap-7.92> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=47 ms TTL=55
Réponse de 1.1.1.1 : octets=32 temps=19 ms TTL=55
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=55
Réponse de 1.1.1.1 : octets=32 temps=25 ms TTL=55

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 47ms, Moyenne = 27ms
```

# II. Exploration locale en duo

Owkay. Vous savez à ce stade :

- afficher les informations IP de votre machine
- modifier les informations IP de votre machine
- c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

## 1. Prérequis

- deux PCs avec ports RJ45
- un câble RJ45
- **firewalls désactivés** sur les deux PCs

## 2. Câblage

Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. **Bap.**

## Création du réseau (oupa)

Cette étape peut paraître cruciale. En réalité, elle n'existe pas à proprement parlé. On ne peut pas "créer" un réseau. Si une machine possède une carte réseau, et si cette carte réseau porte une adresse IP, alors cette adresse IP se trouve dans un réseau (l'adresse de réseau). Ainsi, le réseau existe. De fait.  

**Donc il suffit juste de définir une adresse IP sur une carte réseau pour que le réseau existe ! Bap.**

## 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
  - choisissez une IP qui commence par "192.168"
  - utilisez un /30 (que deux IP disponibles)

![modification-ip](./image/modification-ip.png)

- vérifiez à l'aide de commandes que vos changements ont pris effet

```
C:\Users\sandr>ipconfig /all

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : D4-5D-64-61-2D-19
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::bcdf:9bb0:684d:9e02%2(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.1
   IAID DHCPv6 . . . . . . . . . . . : 47471972
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-2C-DE-87-D4-5D-64-61-2D-19
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

```

- utilisez `ping` pour tester la connectivité entre les deux machines

Tentative de ping sur l'adresse ip de la carte réseau éthernet de Costa : 
``` 
C:\Users\sandr>ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms

```

- affichez et consultez votre table ARP

```
C:\Users\sandr>arp -a

Interface : 192.168.0.2 --- 0x2
  Adresse Internet      Adresse physique      Type
  169.254.134.245       d4-5d-64-5e-0a-a3     dynamique
  192.168.0.1           d4-5d-64-5e-0a-a3     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  230.0.0.1             01-00-5e-00-00-01     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

  [...]

```


## 4. Utilisation d'un des deux comme gateway

Ca, ça peut toujours dépann. Comme pour donner internet à une tour sans WiFi quand y'a un PC portable à côté, par exemple. 

L'idée est la suivante :

- vos PCs ont deux cartes avec des adresses IP actuellement
  - la carte WiFi, elle permet notamment d'aller sur internet, grâce au réseau YNOV
  - la carte Ethernet, qui permet actuellement de joindre votre coéquipier, grâce au réseau que vous avez créé :)
- si on fait un tit schéma tout moche, ça donne ça :

```schema
  Internet           Internet
     |                   |
    WiFi                WiFi
     |                   |
    PC 1 ---Ethernet--- PC 2
    
- internet joignable en direct par le PC 1
- internet joignable en direct par le PC 2
```

- vous allez désactiver Internet sur une des deux machines, et vous servir de l'autre machine pour accéder à internet.

```schema
  Internet           Internet
     X                   |
     X                  WiFi
     |                   |
    PC 1 ---Ethernet--- PC 2
    
- internet joignable en direct par le PC 2
- internet joignable par le PC 1, en passant par le PC 2
```

- pour ce faiiiiiire :
  - désactivez l'interface WiFi sur l'un des deux postes
  - s'assurer de la bonne connectivité entre les deux PCs à travers le câble RJ45
  - **sur le PC qui n'a plus internet**
    - sur la carte Ethernet, définir comme passerelle l'adresse IP de l'autre PC

Mon Pc étant celui que nous avons déconnecté d'internet, je modifie ma passerelle en y mettant l'adresse ip de la carte réseau éthernet de Costa :

![nouvelle-passerrelle](./image/nouvelle-passerelle.png)

  - **sur le PC qui a toujours internet**
    - sur Windows, il y a une option faite exprès (google it. "share internet connection windows 10" par exemple)
    

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait

Test de ping depuis mon ordinateur au DNS 1.1.1.1 en étant connecté au wifi depuis l'ordinateur de Costa :
```
C:\Users\sandr>ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=19 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 19ms, Moyenne = 18ms

```

- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de `netcat` (abrégé `nc`). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.

Sous GNU/Linux et MacOS vous l'avez sûrement déjà, sinon débrouillez-vous pour l'installer :). Les Windowsien, ça se passe [ici](https://eternallybored.org/misc/netcat/netcat-win32-1.11.zip) (from https://eternallybored.org/misc/netcat/).  

Une fois en possession de `netcat`, vous allez pouvoir l'utiliser en ligne de commande. Comme beaucoup de commandes sous GNU/Linux, Mac et Windows, on peut utiliser l'option `-h` (`h` pour `help`) pour avoir une aide sur comment utiliser la commande.  


L'idée ici est la suivante :

- l'un de vous jouera le rôle d'un *serveur*
- l'autre sera le *client* qui se connecte au *serveur*

Précisément, on va dire à `netcat` d'*écouter sur un port*. Des ports, y'en a un nombre fixe (65536, on verra ça plus tard), et c'est juste le numéro de la porte à laquelle taper si on veut communiquer avec le serveur.

Si le serveur écoute à la porte 20000, alors le client doit demander une connexion en tapant à la porte numéro 20000, simple non ?  

Here we go :

- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
  - `nc.exe -l -p 8888`
    - "`netcat`, écoute sur le port numéro 8888 stp"
  - il se passe rien ? Normal, faut attendre qu'un client se connecte
- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  - `nc.exe 192.168.1.1 8888`
    - "`netcat`, connecte toi au port 8888 de la machine 192.168.1.1 stp"
  - une fois fait, vous pouvez taper des messages dans les deux sens
- appelez-moi quand ça marche ! :)
  - si ça marche pas, essayez d'autres options de `netcat`

Mon Pc est le PC Client, le PC de Costa sera le PC serveur : 
```
PS C:\Users\sandr\Documents\netcat-win32-1.11\netcat-1.11> ./nc.exe 192.168.0.1 8888
coucou
coucou
comment ça va?
mal rien ne marche
Mais faut pas désepéré XD
tu vas y arriver

```

- 🌞 pour aller un peu plus loin
  - le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
  - par exemple, on écoute sur l'interface Ethernet, mais pas sur la WiFI
  - pour ce faire `nc.exe -l -p PORT_NUMBER IP_ADDRESS`
  - par exemple `nc.exe -l -p 9999 192.168.1.37`
  - on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur `127.0.0.1`

  Sur mon ordinateur :
  ```
  PS C:\Users\sandr\Documents\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 8888 192.168.0.1
  PS C:\Users\sandr\Documents\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 8888 192.168.0.1
  PS C:\Users\sandr\Documents\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 8888 192.168.0.1
  coucou
  coucou
  test
  abc
  def
  ghi

  ```
  Sur l'ordinateur de Costa :
  ```
  PS C:\Users\reype\Documents\YNOV\B2\Réseau> .\\nc.exe -l -p 8888 127.0.0.1
  >>
  invalid connection to [192.168.0.1] from (UNKNOWN) [192.168.0.2] 53296
  PS C:\Users\reype\Documents\YNOV\B2\Réseau> .\\nc.exe -l -p 8888 127.0.0.1
  invalid connection to [192.168.0.1] from (UNKNOWN) [192.168.0.2] 53297
  PS C:\Users\reype\Documents\YNOV\B2\Réseau> .\\nc.exe -l -p 8888 192.168.0.2
  coucou
  coucou
  test
  abc
  def
  ghi

  ```
  Test d'écoute de sa propre ip sur l'ordinateur de Costa :
  ```
  *Machine 1 Premier terminal*
  PS C:\Users\reype\Documents\YNOV\B2\Réseau> .\\nc.exe -l -p 8888 192.168.0.1
  coucou
  yes
  chat local
  ne fonctionne plus
  AH
  
  *Machine 1 Deuxième terminal*
  PS C:\Users\reype\Documents\YNOV\B2\Réseau> .\nc.exe 192.168.0.1 8888
  coucou
  yes
  chat local
  ne fonctionne plus
  ```

## 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

- Activez votre firewall
- 🌞 Autoriser les `ping`
  - configurer le firewall de votre OS pour accepter le `ping`
  - aidez vous d'internet
  - on rentrera dans l'explication dans un prochain cours mais sachez que `ping` envoie un message *ICMP de type 8* (demande d'ECHO) et reçoit un message *ICMP de type 0* (réponse d'écho) en retour

- 🌞 Autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000
  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne
  
![régles-de-traffic](./image/régles-de-trafic-entrant.png)

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.  

C'est le **serveur DHCP** d'YNOV qui vous a donné une IP.

Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé *bail DHCP* qui contient, entre autres :

- l'IP qu'on vous a donné
- le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP
- vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)

```
C:\Users\sandr>ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 70-66-55-CF-4B-3B
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::bdd5:65ba:fe7a:abf5%18(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.85(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 22:22:53      <-----
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 22:22:54         <-----
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 158361173
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-2C-DE-87-D4-5D-64-61-2D-19
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

```

## 2. DNS

Le protocole DNS permet la résolution de noms de domaine vers des adresses IP. Ce protocole permet d'aller sur `google.com` plutôt que de devoir connaître et utiliser l'adresse IP du serveur de Google.  

Un **serveur DNS** est un serveur à qui l'on peut poser des questions (= effectuer des requêtes) sur un nom de domaine comme `google.com`, afin d'obtenir les adresses IP liées au nom de domaine.  

Si votre navigateur fonctionne "normalement" (il vous permet d'aller sur `google.com` par exemple) alors votre ordinateur connaît forcément l'adresse d'un serveur DNS. Et quand vous naviguez sur internet, il effectue toutes les requêtes DNS à votre place, de façon automatique.

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
C:\Users\sandr>ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 70-66-55-CF-4B-3B
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::bdd5:65ba:fe7a:abf5%18(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.85(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 22:22:53
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 22:22:54
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 158361173
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-2C-DE-87-D4-5D-64-61-2D-19
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1    <----
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* (*lookup* = "dis moi à quelle IP se trouve tel nom de domaine")
    - pour `google.com`
    ```
    C:\Users\sandr>nslookup google.com
    Serveur :   box
    Address:  192.168.1.1

    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:80d::200e
              172.217.22.142
    ```

    - pour `ynov.com`

    ```
    C:\Users\sandr>nslookup ynov.com
    Serveur :   box
    Address:  192.168.1.1

    Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Address:  92.243.16.143
    ```

    - interpréter les résultats de ces commandes

Cette commande envoie une requête au serveur DNS qui nous renvoie l'ip correspondant à notre recherche.

  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes

L'adresse IP à laquelle je fais la requête est ```192.168.1.1```

  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
    - pour l'adresse `78.74.21.21`
    ```
    C:\Users\sandr>nslookup 78.74.21.21
    Serveur :   box
    Address:  192.168.1.1

    Nom :    host-78-74-21-21.homerun.telia.com
    Address:  78.74.21.21
    ```

    - pour l'adresse `92.146.54.88`
    ```
    C:\Users\sandr>nslookup 92.146.54.88
    Serveur :   box
    Address:  192.168.1.1

    Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
    Address:  92.146.54.88
    ```
    - interpréter les résultats

Cette commande envoie une requête au serveur DNS qui nous renvoie le nom de domaine correspondant à notre recherche.

    - *si vous vous demandez, j'ai pris des adresses random :)*

# IV. Wireshark

Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.

Il peut :

- enregistrer le trafic réseau, pour l'analyser plus tard
- afficher le trafic réseau en temps réel

**On peut TOUT voir.**

Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

- téléchargez l'outil [Wireshark](https://www.wireshark.org/)
- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle

![ping](./image/ping-passerelle.png)

  - un `netcat` entre vous et votre mate, branché en RJ45

Ping de costa vers mon ordi :
![ping costa](./image/ping-de-costa.png)

Ping de mon ordi vers costa :
![mon ping](./image/mon-ping-de-costa.png) 
- une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
  - prenez moi des screens des trames en question
  - on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe

![requête DNS](./image/requete-dns.png)

La requête est réalisé au serveur DNS avec l'ip ```192.168.1.1```

# Bilan

**Vu pendant le TP :**

- visualisation de vos interfaces réseau (en GUI et en CLI)
- extraction des informations IP
  - adresse IP et masque
  - calcul autour de IP : adresse de réseau, etc.
- connaissances autour de/aperçu de :
  - un outil de diagnostic simple : `ping`
  - un outil de scan réseau : `nmap`
  - un outil qui permet d'établir des connexions "simples" (on y reviendra) : `netcat`
  - un outil pour faire des requêtes DNS : `nslookup` ou `dig`
  - un outil d'analyse de trafic : `wireshark`
- manipulation simple de vos firewalls

**Conclusion :**

- Pour permettre à un ordinateur d'être connecté en réseau, il lui faut **une liaison physique** (par câble ou par *WiFi*).  
- Pour réceptionner ce lien physique, l'ordinateur a besoin d'**une carte réseau**. La carte réseau porte une [adresse MAC](../../cours/lexique.md#mac-media-access-control).  
- **Pour être membre d'un réseau particulier, une carte réseau peut porter une adresse IP.**
Si deux ordinateurs reliés physiquement possèdent une adresse IP dans le même réseau, alors ils peuvent communiquer.  
- **Un ordintateur qui possède plusieurs cartes réseau** peut réceptionner du trafic sur l'une d'entre elles, et le balancer sur l'autre, servant ainsi de "pivot". Cet ordinateur **est appelé routeur**.
- Il existe dans la plupart des réseaux, certains équipements ayant un rôle particulier :
  - un équipement appelé **[*passerelle*](../../cours/lexique.md#passerelle-ou-gateway)**. C'est un routeur, et il nous permet de sortir du réseau actuel, pour en joindre un autre, comme Internet par exemple
  - un équipement qui agit comme **serveur DNS** : il nous permet de connaître les IP derrière des noms de domaine
  - un équipement qui agit comme **serveur DHCP** : il donne automatiquement des IP aux clients qui rejoigne le réseau
  - **chez vous, c'est votre Box qui fait les trois :)**

🌞 Ce soleil est un troll. **Lisez et prenez le temps d'appréhender le texte de conclusion juste au dessus si ces notions ne vous sont pas familières.**
