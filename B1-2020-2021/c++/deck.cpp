
#include "deck.h"
#include "card.h"
#include <iostream>     
#include <cstdio>      
#include <cstdlib>     
#include <ctime>
#include <vector>
#include <sstream>
#include <algorithm> 
using namespace std;      


Deck::Deck(int fullDeck,vector<Card> koikoiDeck): m_numCards(fullDeck), m_cardDeck(koikoiDeck)
{
}

Card*Deck::enleve()
{
    Card *tempCard;
    tempCard = &m_cardDeck[m_numCards-1];
    m_cardDeck.pop_back();
    m_numCards--;
    return tempCard;
}

int Deck::getNumCards()
{
    return m_numCards;
}

void Deck::shuffleDeck()
{

    int i {0};
    int r {0};

    for (i = 0; i < m_numCards; i++)
    {
        r = static_cast<int>(rand() % m_numCards);
        swap(m_cardDeck[i], m_cardDeck[r]);
    }
    cout << "=======================" << endl;
    cout << "Le paquet a été mélangé" << endl;
    cout << "=======================" << endl;
}

