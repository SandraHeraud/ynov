#ifndef CARD_H
#define CARD_H

#include <string>


enum CardMonth
{
    JANVIER,
    FEVRIER,
    MARS,
    AVRIL,
    MAI,
    JUIN,
    JUILLET,
    AOUT,
    SEPTEMBRE,
    OCTOBRE,
    NOVEMBRE,
    DECEMBRE
};

enum CardFlower
{
    PIN,
    PRUNIER,
    CERISIER,
    GLYCINE,
    IRIS,
    PIVOINE,
    TREFLE,
    PAMPAS,
    CHRYSANTHEME,
    ERABLE,
    SAULE,
    PAULOWNIA
};

enum CardType
{
    LUMIERE,
    GRUE,
    RIDEAU,
    PLUIE,
    LUNE,
    PHENIX,
    ANIMAL,
    ROSSIGNOL,
    COUCOU,
    PONT,
    PAPILLON,
    SANGLIER,
    OIES,
    COUPE_SAKE,
    BICHE,
    HIRONDELLE,
    RUBAN,
    RUBAN_ROUGE,
    RUBAN_ROUGE_POEME,
    RUBAN_BLEU,
    PLAINE,
    NIL
};

class Card
{

private:
    
    CardMonth m_month;
    CardFlower m_flower;
    CardType m_cardType,m_cardType2;
    int m_point, m_point2;

public:
    
    Card(CardMonth month, CardFlower flower, CardType cardType, CardType cardType2, int point, int point2);
    CardMonth getMonth();
    CardFlower getFlower();
    CardType getCardType();
    CardType getCardType2();
    int getPoint();
    int getPoint2();
};

#endif
