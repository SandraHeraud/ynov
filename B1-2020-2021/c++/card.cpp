#include "card.h"
#include <iostream>
#include <string>


Card::Card(CardMonth month, CardFlower flower, CardType cardType, CardType cardType2, int point, int point2):
    m_month(month),m_flower(flower),m_cardType(cardType),m_cardType2(cardType2),m_point(point),m_point2(point2)
{
    
}



CardMonth Card::getMonth()
{
    return m_month;
}

CardFlower Card::getFlower()
{
    return m_flower;
}

CardType Card::getCardType()
{
    return m_cardType;
}

CardType Card::getCardType2()
{
    return m_cardType2;
}

int Card::getPoint()
{
    return m_point;
}

int Card::getPoint2()
{
    return m_point2;
}