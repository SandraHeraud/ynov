#ifndef DECK_H
#define DECK_H

#include "card.h"
#include <string>
#include <vector>
using namespace std;

constexpr static int fullDeck {48};

const static vector<Card> koikoiDeck
{
    Card(JANVIER, PIN, LUMIERE, GRUE, 20, 0),
    Card(JANVIER, PIN, RUBAN, RUBAN_ROUGE_POEME, 5, 0),
    Card(JANVIER, PIN, PLAINE, NIL, 1, 0),
    Card(JANVIER, PIN, PLAINE, NIL, 1, 0),
    Card(FEVRIER, PRUNIER, ANIMAL, ROSSIGNOL, 10, 0),
    Card(FEVRIER, PRUNIER, RUBAN, RUBAN_ROUGE_POEME, 5, 0),
    Card(FEVRIER, PRUNIER, PLAINE, NIL, 1, 0 ),
    Card(FEVRIER, PRUNIER, PLAINE, NIL, 1, 0 ),
    Card(MARS, CERISIER, LUMIERE, RIDEAU, 20, 0),
    Card(MARS, CERISIER, RUBAN, RUBAN_ROUGE_POEME, 5, 0),
    Card(MARS, CERISIER, PLAINE, NIL, 1, 0 ),
    Card(MARS, CERISIER, PLAINE, NIL, 1, 0 ),
    Card(AVRIL, GLYCINE, ANIMAL, COUCOU, 10, 0),
    Card(AVRIL, GLYCINE, RUBAN, RUBAN_ROUGE, 5, 0),
    Card(AVRIL, GLYCINE, PLAINE, NIL, 1, 0 ),
    Card(AVRIL, GLYCINE, PLAINE, NIL, 1, 0 ),
    Card(MAI, IRIS, ANIMAL, PONT, 10, 0),
    Card(MAI, IRIS, RUBAN, RUBAN_ROUGE, 5, 0),
    Card(MAI, IRIS, PLAINE, NIL, 1, 0 ),
    Card(MAI, IRIS, PLAINE, NIL, 1, 0 ),
    Card(JUIN, PIVOINE, ANIMAL, PAPILLON, 10, 0),
    Card(JUIN, PIVOINE, RUBAN, RUBAN_BLEU, 5, 0),
    Card(JUIN, PIVOINE, PLAINE, NIL, 1, 0 ),
    Card(JUIN, PIVOINE, PLAINE, NIL, 1, 0 ),
    Card(JUILLET, TREFLE, ANIMAL, SANGLIER, 10, 0),
    Card(JUILLET, TREFLE, RUBAN, RUBAN_ROUGE, 5, 0),
    Card(JUILLET, TREFLE, PLAINE, NIL, 1, 0 ),
    Card(JUILLET, TREFLE, PLAINE, NIL, 1, 0 ),
    Card(AOUT, PAMPAS, LUMIERE, LUNE, 20, 0),
    Card(AOUT, PAMPAS, ANIMAL, OIES, 10, 0),
    Card(AOUT, PAMPAS, PLAINE, NIL, 1, 0),
    Card(AOUT, PAMPAS, PLAINE, NIL, 1, 0),
    Card(SEPTEMBRE, CHRYSANTHEME, ANIMAL, COUPE_SAKE, 10, 0),
    Card(SEPTEMBRE, CHRYSANTHEME, RUBAN, RUBAN_BLEU, 5, 0),
    Card(SEPTEMBRE, CHRYSANTHEME, PLAINE, NIL, 1, 0),
    Card(SEPTEMBRE, CHRYSANTHEME, PLAINE, NIL, 1, 0),
    Card(OCTOBRE, ERABLE, ANIMAL, BICHE, 10, 0),
    Card(OCTOBRE, ERABLE, RUBAN, RUBAN_BLEU, 5, 0),
    Card(OCTOBRE, ERABLE, PLAINE, NIL, 1, 0 ),
    Card(OCTOBRE, ERABLE, PLAINE, NIL, 1, 0 ),
    Card(NOVEMBRE, SAULE, LUMIERE, PLUIE, 20, 0),
    Card(NOVEMBRE, SAULE, ANIMAL, HIRONDELLE, 10, 0),
    Card(NOVEMBRE, SAULE, RUBAN, RUBAN_ROUGE, 5, 0),
    Card(NOVEMBRE, SAULE, PLAINE, NIL, 1, 0 ),
    Card(DECEMBRE, PAULOWNIA, LUMIERE, PHENIX, 20, 0),
    Card(DECEMBRE, PAULOWNIA, PLAINE, NIL, 1, 0 ),
    Card(DECEMBRE, PAULOWNIA, PLAINE, NIL, 1, 0 ),
    Card(DECEMBRE, PAULOWNIA, PLAINE, NIL, 1, 0 )
};

class Deck
{

private:
    int m_numCards {fullDeck};
    vector<Card> m_cardDeck {koikoiDeck};
;
public:
    
    Deck(int fullDeck,vector<Card> koikoiDeck);
    
    Card* enleve();
    int getNumCards();
    void shuffleDeck();

   
};

#endif