# TP - 1

# 1. Self-footprinting

### Host-Os
Pour trouver les informations de la machine, on effectue la commande ``systeminfo``.
```
PS C:\> systeminfo

Nom de l’hôte:                              DESKTOP-7C6EJL1
Nom du système d’exploitation:              Microsoft Windows 10 Famille      
Version du système:                         10.0.19041 N/A version 19041
Fabricant du système d’exploitation:        Microsoft Corporation
Configuration du système d’exploitation:    Station de travail autonome
Type de version du système d’exploitation:  Multiprocessor Free
Propriétaire enregistré:                    sandra.heraud@sfr.fr
Organisation enregistrée:
Identificateur de produit:                  00326-10118-31079-AA594
Date d’installation originale:              29/10/2020, 21:26:34
Heure de démarrage du système:              30/10/2020, 17:20:38
Fabricant du système:                       ASUSTeK COMPUTER INC.
Modèle du système:                          TUF Gaming FX505DT_TUF505DT
Type du système:                            x64-based PC
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : AMD64 Family 23 Model 24 Stepping 1 AuthenticAMD ~1400 MHz
Version du BIOS:                            American Megatrends Inc. FX505DT.310, 24/12/2019
Répertoire Windows:                         C:\Windows
Répertoire système:                         C:\Windows\system32
Périphérique d’amorçage:                    \Device\HarddiskVolume1
Option régionale du système:                fr;Français (France)
Paramètres régionaux d’entrée:              fr;Français (France)
Fuseau horaire:                             (UTC+01:00) Bruxelles, Copenhague, Madrid, Paris
Mémoire physique totale:                    7 997 Mo
Mémoire physique disponible:                3 012 Mo
Mémoire virtuelle : taille maximale:        9 917 Mo
Mémoire virtuelle : disponible:             2 357 Mo
Mémoire virtuelle : en cours d’utilisation: 7 560 Mo
Emplacements des fichiers d’échange:        C:\pagefile.sys
Domaine:                                    WORKGROUP
Serveur d’ouverture de session:             \\DESKTOP-7C6EJL1
Correctif(s):                               6 Corrections installées.
                                            [01]: KB4578968
                                            [02]: KB4537759
                                            [03]: KB4557968
                                            [04]: KB4577266
                                            [05]: KB4580325
                                            [06]: KB4579311
Carte(s) réseau:                            3 carte(s) réseau installée(s).
                                            [01]: Bluetooth Device (Personal Area Network)
                                                  Nom de la connexion : Connexion réseau Bluetooth
                                                  État :                Support déconnecté
                                            [02]: Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.0.1
                                                  Adresse(s) IP
                                                  [01]: 192.168.0.22
                                                  [02]: fe80::bdd5:65ba:fe7a:abf5
                                            [03]: Realtek PCIe GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
Configuration requise pour Hyper-V:         Extensions de mode du moniteur d’ordinateur virtuel : Oui
                                            Virtualisation activée dans le microprogramme : Non
                                            Traduction d’adresse de second niveau : Oui
                                            Prévention de l’exécution des données disponible : Oui
```

Pour obtenir le model de la RAM on effectue la commande ``wmic MemoryChip list full``.
```
PS C:\> wmic MemoryChip list full


BankLabel=P0 CHANNEL A
Capacity=8589934592
DataWidth=64
Description=Mémoire physique
DeviceLocator=DIMM 0
FormFactor=12
HotSwappable=
InstallDate=
InterleaveDataDepth=0
InterleavePosition=0
Manufacturer=Hynix
MemoryType=0
Model=
Name=Mémoire physique
OtherIdentifyingInfo=
PartNumber=HMA81GS6JJR8N-VK
PositionInRow=
PoweredOn=
Removable=
Replaceable=
SerialNumber=34A51E77
SKU=
Speed=2667
Status=
Tag=Physical Memory 0
TotalWidth=64
TypeDetail=16512
Version=


```

## Devices
Pour trouver les informations du processeur, on effectue la commande ``Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*``.
```

PS C:\> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*

Name                                            NumberOfCores NumberOfEnabledCore NumberOfLogicalProcessors
----                                            ------------- ------------------- -------------------------
AMD Ryzen 5 3550H with Radeon Vega Mobile Gfx               4                   4                         8

```

Pour obtenir les détails des disques, on effectue la commande ``diskpart`` pour obtenir les informations de disques.
On selectionne le dique avec ``sel disk <n>``\ (n : numéro du disque). Puis on effectue la commande ``detail disk`` pour obtenir les informations.
```

DISKPART> sel disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk

Micron_2200V_MTFDHBA512TCK
ID du disque : {F483A303-4139-4CBE-B9B2-64A7E5CDE440}
Type : NVMe
État : En ligne
Chemin : 0
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(0103)#PCI(0000)#NVME(P00T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Oui
Disque de fichiers d’échange : Oui
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Oui
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C                NTFS   Partition    476 G   Sain       Démarrag
  Volume 1                      NTFS   Partition    519 M   Sain
  Volume 2                      FAT32  Partition    100 M   Sain       Système

```

## Users
On obtient les informations des utilisateurs avec la commande ``net users``.
```
PS C:\> net user

comptes d’utilisateurs de \\DESKTOP-7C6EJL1

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           Invité
sandr                    WDAGUtilityAccount       
La commande s’est terminée correctement.

```

## Processus
La liste des processus s'obtient avec la commande ``tasklist``.
```
PS C:\> tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation 
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     4 728 Ko
Registry                       124 Services                   0    29 784 Ko
smss.exe                       492 Services                   0       516 Ko
csrss.exe                      780 Services                   0     3 956 Ko
wininit.exe                    916 Services                   0     3 760 Ko
services.exe                   560 Services                   0     8 068 Ko
lsass.exe                      752 Services                   0    19 748 Ko
svchost.exe                   1124 Services                   0     1 324 Ko
svchost.exe                   1148 Services                   0    25 976 Ko
fontdrvhost.exe               1176 Services                   0     1 032 Ko
svchost.exe                   1272 Services                   0    15 576 Ko
[...]
```
5 services 

``svchost.exe`` 

Permet de charger les bibliothèques de liens dynamiques. Il est l'hôte des services d'où son nom : ``svc (services) host (hôte)``

``lsass.exe`` 

(Local Security Authority Subsystem Service)
Assure l'identification des utilisateurs windows sur ce système local.

``wininit.exe``

winint comme son nom l'indique permet l'initialisation de windows (**Win**dows **Init**ialization)

``smss.exe``

(Session Manager Subsystem)
    * smss.exe est lancé dés le processus de démarrage windows. Il permet de vérifier la hiérarchie des fichiers systèmes et de créer des variables d'environnements. Enfin une fois les variables créer il démarre plusieurs processus tel que la gestion de mémoire, le mode noyau du sous system win32 et la connexion à l'utilisateur avec csrss.exe.
``csrss.exe``

(Client/Server Runtime Subsystem)
Il permet de gérer les éléments graphiques de windows (fenêtres).

## Network

On obtient les informations des cartes réseaux avec la commande ``Get-NetAdapter`` :
```
PS C:\> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Wi-Fi                     Realtek 8821CE Wireless LAN 802.11ac...      19 Up           70-66-55-CF-4B-3B      72.2 Mbps
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter        16 Up           0A-00-27-00-00-10         1 Gbps
VirtualBox Host-Only ...2 VirtualBox Host-Only Ethernet Adap...#2      15 Up           0A-00-27-00-00-0F         1 Gbps
VirtualBox Host-Only ...3 VirtualBox Host-Only Ethernet Adap...#3       9 Up           0A-00-27-00-00-09         1 Gbps
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...       4 Disconnected 70-66-55-CF-4B-3A         3 Mbps
Ethernet                  Realtek PCIe GbE Family Controller            2 Disconnected D4-5D-64-61-2D-19          0 bps

```
Bluetooth Device (Personal Area Netw... : 
Il s'agit de la carte permettant connexion Bluetooth. On voit qu'elle est déconnectée et qu'elle a une vitesse de transfert de 3 Mbps.

Realtek PCIe GBE Family Controller :
Elle permet la connexion direct en RJ-45 à internet. Elle est déconnectée.

Pour obtenir les port TCP actifs, on effectue la commande ``Get-NetTCPConnection`` :
```

PS C:\> Get-NetTCPConnection

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting OwningProcess 
------------                        --------- -------------                       ---------- -----       -------------- -------------
::                                  49671     ::                                  0          Listen                     560
::                                  49668     ::                                  0          Listen                     3892
::                                  49667     ::                                  0          Listen                     1736
::                                  49666     ::                                  0          Listen                     1940
::                                  49665     ::                                  0          Listen                     916
::                                  49664     ::                                  0          Listen                     752
::                                  12177     ::                                  0          Listen                     9612
::                                  9013      ::                                  0          Listen                     3176
::                                  9012      ::                                  0          Listen                     3176
::                                  7680      ::                                  0          Listen                     17288
::                                  1043      ::                                  0          Listen                     13000
::                                  1042      ::                                  0          Listen                     13000
::                                  445       ::                                  0          Listen                     4
::                                  135       ::                                  0          Listen                     1272
0.0.0.0                             65289     0.0.0.0                             0          Bound                      17264
0.0.0.0                             65212     0.0.0.0                             0          Bound                      10844
0.0.0.0                             65166     0.0.0.0                             0          Bound                      5096
0.0.0.0                             65158     0.0.0.0                             0          Bound                      10360
127.0.0.1                           55900     0.0.0.0                             0          Bound                      59348
0.0.0.0                             55777     0.0.0.0                             0          Bound                      10840
127.0.0.1                           55752     0.0.0.0                             0          Bound                      59348
127.0.0.1                           55738     0.0.0.0                             0          Bound                      59348
0.0.0.0                             55737     0.0.0.0                             0          Bound                      10840
0.0.0.0                             55736     0.0.0.0                             0          Bound                      3176
0.0.0.0                             53583     0.0.0.0                             0          Bound                      4728
0.0.0.0                             49745     0.0.0.0                             0          Bound                      55820
0.0.0.0                             49741     0.0.0.0                             0          Bound                      54792
0.0.0.0                             49740     0.0.0.0                             0          Bound                      17288
0.0.0.0                             49674     0.0.0.0                             0          Bound                      55408
0.0.0.0                             49653     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49651     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49635     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49621     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49508     0.0.0.0                             0          Bound                      54792
0.0.0.0                             49507     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49357     0.0.0.0                             0          Bound                      10844
0.0.0.0                             49341     0.0.0.0                             0          Bound                      70080
0.0.0.0                             49262     0.0.0.0                             0          Bound                      68244
0.0.0.0                             49256     0.0.0.0                             0          Bound                      70080
0.0.0.0                             49239     0.0.0.0                             0          Bound                      68164
0.0.0.0                             49232     0.0.0.0                             0          Bound                      55564
0.0.0.0                             49230     0.0.0.0                             0          Bound                      70080
0.0.0.0                             49219     0.0.0.0                             0          Bound                      69924
192.168.0.22                        65289     52.113.199.201                      443        Established Internet       17264
192.168.0.22                        65212     35.186.224.47                       443        Established Internet       10844
192.168.0.22                        65166     40.67.254.36                        443        Established Internet       5096
127.0.0.1                           65158     127.0.0.1                           55748      Established Internet       10360
127.0.0.1                           65001     0.0.0.0                             0          Listen                     4728
127.0.0.1                           65001     127.0.0.1                           53583      Established Internet       4728
127.0.0.1                           55900     127.0.0.1                           13030      Established Internet       59348
127.0.0.1                           55777     127.0.0.1                           9012       Established Internet       10840
127.0.0.1                           55752     127.0.0.1                           17945      Established Internet       59348
127.0.0.1                           55748     0.0.0.0                             0          Listen                     15800
127.0.0.1                           55748     127.0.0.1                           65158      Established Internet       15800
127.0.0.1                           55738     127.0.0.1                           9487       Established Internet       59348
127.0.0.1                           55737     127.0.0.1                           9487       Established Internet       10840
127.0.0.1                           55736     127.0.0.1                           1043       Established Internet       3176
127.0.0.1                           53583     127.0.0.1                           65001      Established Internet       4728
127.0.0.1                           51947     0.0.0.0                             0          Listen                     4324
192.168.0.22                        49745     20.44.232.74                        443        Established Internet       55820
192.168.0.22                        49744     178.32.154.10                       443        TimeWait                   0
192.168.0.22                        49741     52.113.205.133                      443        Established Internet       54792
192.168.0.22                        49740     20.54.24.169                        443        Established Internet       17288
192.168.0.22                        49739     95.100.252.24                       80         TimeWait                   0
192.168.0.22                        49716     172.217.22.131                      443        TimeWait                   0
192.168.0.22                        49710     216.58.213.162                      443        TimeWait                   0
192.168.0.22                        49704     172.217.19.228                      443        TimeWait                   0
192.168.0.22                        49702     216.58.209.238                      443        TimeWait                   0
192.168.0.22                        49700     216.58.214.67                       443        TimeWait                   0
192.168.0.22                        49699     216.58.213.168                      443        TimeWait                   0
192.168.0.22                        49682     151.139.237.11                      443        TimeWait                   0
192.168.0.22                        49680     104.16.85.20                        443        TimeWait                   0
192.168.0.22                        49676     172.217.19.234                      443        TimeWait                   0
127.0.0.1                           49674     127.0.0.1                           49673      Established Internet       55408
127.0.0.1                           49673     127.0.0.1                           49674      Established Internet       55408
0.0.0.0                             49671     0.0.0.0                             0          Listen                     560
0.0.0.0                             49668     0.0.0.0                             0          Listen                     3892
0.0.0.0                             49667     0.0.0.0                             0          Listen                     1736
0.0.0.0                             49666     0.0.0.0                             0          Listen                     1940
0.0.0.0                             49665     0.0.0.0                             0          Listen                     916
0.0.0.0                             49664     0.0.0.0                             0          Listen                     752
192.168.0.22                        49661     216.58.206.234                      443        TimeWait                   0
192.168.0.22                        49653     13.249.7.7                          443        Established Internet       10844
192.168.0.22                        49651     99.86.95.32                         443        Established Internet       10844
192.168.0.22                        49635     162.159.138.234                     443        Established Internet       10844
192.168.0.22                        49621     162.159.133.232                     443        Established Internet       10844
192.168.0.22                        49508     52.113.199.90                       443        Established Internet       54792
192.168.0.22                        49507     162.159.137.232                     443        Established Internet       10844
192.168.0.22                        49357     162.159.136.234                     443        Established Internet       10844
192.168.0.22                        49341     18.179.85.10                        443        Established Internet       70080
127.0.0.1                           49262     127.0.0.1                           49261      Established Internet       68244
127.0.0.1                           49261     127.0.0.1                           49262      Established Internet       68244
192.168.0.22                        49256     52.12.8.165                         443        Established Internet       70080
127.0.0.1                           49239     127.0.0.1                           49238      Established Internet       68164
127.0.0.1                           49238     127.0.0.1                           49239      Established Internet       68164
127.0.0.1                           49232     127.0.0.1                           49231      Established Internet       55564
127.0.0.1                           49231     127.0.0.1                           49232      Established Internet       55564
127.0.0.1                           49230     127.0.0.1                           49229      Established Internet       70080
127.0.0.1                           49229     127.0.0.1                           49230      Established Internet       70080
192.168.0.22                        49219     52.236.190.37                       443        Established Internet       69924
127.0.0.1                           17945     127.0.0.1                           55752      Established Internet       10840
127.0.0.1                           17945     0.0.0.0                             0          Listen                     10840
127.0.0.1                           13032     0.0.0.0                             0          Listen                     10840
127.0.0.1                           13031     0.0.0.0                             0          Listen                     10840
127.0.0.1                           13030     0.0.0.0                             0          Listen                     4792
127.0.0.1                           13030     127.0.0.1                           55900      Established Internet       4792
127.0.0.1                           13010     0.0.0.0                             0          Listen                     4372
127.0.0.1                           9487      0.0.0.0                             0          Listen                     4372
127.0.0.1                           9487      127.0.0.1                           55738      Established Internet       4372
127.0.0.1                           9487      127.0.0.1                           55737      Established Internet       4372
127.0.0.1                           9012      127.0.0.1                           55777      Established Internet       3176
127.0.0.1                           6463      0.0.0.0                             0          Listen                     57584
0.0.0.0                             5040      0.0.0.0                             0          Listen                     8300
127.0.0.1                           1043      127.0.0.1                           55736      Established Internet       13000
192.168.120.2                       139       0.0.0.0                             0          Listen                     4
192.168.56.1                        139       0.0.0.0                             0          Listen                     4
192.168.0.22                        139       0.0.0.0                             0          Listen                     4
10.10.10.1                          139       0.0.0.0                             0          Listen                     4
0.0.0.0                             135       0.0.0.0                             0          Listen                     1272

```

# 2. Scripting

```
PS C:\Users\sandr\Desktop\Code\window client\workstation> .\tp1_script1.ps1

Nom de la Machine : DESKTOP-7C6EJL1

Date et heure d'allumage :
dimanche 15 novembre 2020 16:18:00

IP Principale : 192.168.0.22

OS : Microsoft Windows 10 Famille
Versin de l'OS : 10.0.19042
OS a jour : NON

RAM (en Go) :
    - Utilise : 5,26310729980469
    - Libre   : 2,1717643737793
    - Total   : 7,43487167358398

Disque (en Go) :
    - Utilise : 127,690353393555
    - Libre   : 348,626766204834
    - Total   : 476,317119598389

Utilisateurs de la Machine :
Administrateur
DefaultAccount
Invité
sandr
WDAGUtilityAccount

Temps de reponse moyen en ms : ping 8.8.8.8 : 59,75
```

# 3. Gestion des softs

L'intéret d'utiliser un gestionnaire de paquets est que par rapport à un téléchargement sur internet le téléchargement est plus rapide, votre identité est protégé car les paquets sont stockés sur des serveurs tiers et les paquets ne possédent pas de virus.

Sur windows, on utilise ```choco list -l``` pour obtenir la liste des paquets installés.
```
PS C:\Users\sandr\Desktop\Code\window client\workstation> choco list -l

Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```
Pour obtenir les infos d'un paquet et connaitre sa provenance, on effectue ```choco info <paquet>```

``` 
PS C:\Users\sandr\Desktop\Code\window client\workstation> choco info Chocolatey        

Chocolatey v0.10.15
chocolatey 0.10.15 [Approved]
 Title: Chocolatey | Published: 03/06/2019
 Package approved as a trusted package on août 22 2020 23:00:51.      
 Package testing status: Passing on juin 03 2019 22:25:10.
 Number of Downloads: 217323037 | Downloads for this version: 51867227
 Package url
 Chocolatey Package Source: n/a
 Package Checksum: 'o4DWQwYd3IxOBkNwzOaIo+PwkFj0Fy0F72UmB4OWDPZ7wUJ97ZeYeWOOycQFu9rFD7DMmZA5QZBcnvgiMcwdaw==' (SHA512)
 Tags: nuget apt-get machine repository chocolatey
 Software Site: https://github.com/chocolatey/choco
 Software License: https://raw.githubusercontent.com/chocolatey/choco/master/LICENSE
 Summary: Chocolatey is the package manager for Windows (like apt-get but for Windows)
 Description: Chocolatey is a package manager for Windows (like apt-get but for Windows). It was designed to be a decentralized framework for quickly installing applications and tools that you need. It is built on the NuGet infrastructure currently using PowerShell as its focus for delivering packages from the distros to your door, err computer.       

  Chocolatey is brought to you by the work and inspiration of the community, the work and thankless nights of the [Chocolatey Team](https://github.com/orgs/chocolatey/people), with Rob heading up the direction.

  You can host your own sources and add them to Chocolatey, you can extend Chocolatey's capabilities, and folks, it's only going to get better.

  ### Information

   * [Chocolatey Website and Community Package Repository](https://chocolatey.org)
   * [Mailing List](http://groups.google.com/group/chocolatey) / [Release Announcements Only Mailing List](https://groups.google.com/group/chocolatey-announce) / [Build Status Mailing List](http://groups.google.com/group/chocolatey-build-status)
   * [Twitter](https://twitter.com/chocolateynuget) / [Facebook](https://www.facebook.com/ChocolateySoftware) / [Github](https://github.com/chocolatey)
   * [Blog](https://chocolatey.org/blog) / [Newsletter](https://chocolatey.us8.list-manage1.com/subscribe?u=86a6d80146a0da7f2223712e4&id=73b018498d)
   * [Documentation](https://chocolatey.org/docs) / [Support](https://chocolatey.org/support)

  ### Commands
  There are quite a few commands you can call - you should check out the [command reference](https://chocolatey.org/docs/commands-reference). Here are the most common:

   * Help - choco -? or choco command -?
   * Search - choco search something
   * List - choco list -lo
   * Config - choco config list
   * Install - choco install baretail
   * Pin - choco pin windirstat
   * Outdated - choco outdated
   * Upgrade - choco upgrade baretail
   * Uninstall - choco uninstall baretail

  #### Alternative installation sources:
   * Install ruby gem - choco install compass -source ruby
   * Install python egg - choco install sphynx -source python
   * Install windows feature - choco install IIS -source windowsfeatures
   * Install webpi feature - choco install IIS7.5Express -source webpi

  #### More
  For more advanced commands and switches, use `choco -?` or `choco command -h`. You can also look at the [command reference](https://chocolatey.org/docs/commands-reference), including how you can force a package to install the x86 version of a package.

  ### Create Packages?
  We have some great guidance on how to do that. Where? I'll give you a hint, it rhymes with socks! [Docs!](https://chocolatey.org/docs/create-packages)

  In that mess there is a link to the [PowerShell Chocolatey module reference](https://chocolatey.org/docs/helpers-reference).
 Release Notes: See all - https://github.com/chocolatey/choco/blob/stable/CHANGELOG.md

1 packages found.
```

# 4. Partage de fichier

Depuis la VM
```
[root@localhost partage]# ls
test  test4
```

Dapuis mon ordinateur

```
PS C:\Users\sandr\Desktop\share> ls


    Répertoire : C:\Users\sandr\Desktop\share


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        15/11/2020     18:13                test
d-----        15/11/2020     18:34                test4


```