# Sandra HERAUD
# 19/10/2020
# récupération de données 

# nom machine
Write-Host -NoNewline "Nom de la Machine : "
$systeminfo = $([system.environment]::MachineName)
$systeminfo

# Date et heure d'allumage
Write-Host -Separator ""
Write-Host -NoNewline "Date et heure d'allumage : " 
$date = $(Get-CimInstance -ClassName Win32_OperatingSystem)
$date.LastBootUpTime

# IP principale
Write-Host -Separator ""
Write-Host -NoNewline "IP Principale : "
$ip = $(Get-NetIPAddress -InterfaceAlias "Wi-fi" -AddressFamily "IPv4")
$ip.IPAddress

# OS et version de l'OS
Write-Host -Separator ""
Write-Host -NoNewline "OS : "
$os = $(Get-ComputerInfo)
$os.osname
Write-Host -NoNewline "Versin de l'OS : "
$os.osversion

# Détermine si l'OS est à jour
Write-Host -NoNewline "OS a jour : "
$Updates = New-Object -ComObject Microsoft.Update.Session
$Updates = $Updates.CreateupdateSearcher()
$Updates = $($Updates.Search("IsHidden=0 and IsInstalled=0").Updates) | Select-Object -ExpandProperty Title
if ($Updates -eq $nill)
{
$Updates = "OUI"
}
else
{
$Updates = "NON"
}
$Updates

# Espace RAM utilisé / Espace RAM dispo
Write-Host -Separator ""
Write-Host "RAM (en Go) : "
$ram = $(Get-CIMInstance -ClassName Win32_OperatingSystem)
Write-Host -NoNewline "    - Utilise : "
$ramUtilise = ($ram.TotalVisibleMemorySize - $ram.FreePhysicalMemory)/1Mb
$ramUtilise
Write-Host -NoNewline "    - Libre   : "
$ram.FreePhysicalMemory/1Mb
Write-Host -NoNewline "    - Total   : "
$ram.TotalVisibleMemorySize/1Mb

# Espace disque utilisé / Espace disque dispo
Write-Host -Separator ""
Write-Host "Disque (en Go) : "
$disque = $(Get-CIMInstance -ClassName Win32_LogicalDisk)
Write-Host -NoNewline "    - Utilise : "
$disqueUtilise = ($disque.Size - $disque.FreeSpace)/1Gb
$disqueUtilise
Write-Host -NoNewline "    - Libre   : "
$disque.FreeSpace/1Gb
Write-Host -NoNewline "    - Total   : "
$disque.Size/1Gb

# Utilisateur de la machine
Write-Host -Separator ""
Write-Host "Utilisateurs de la Machine : "
$utilisateur = $(Get-LocalUser)
$utilisateur.Name


# calcul et affiche le temps de réponse moyen vers 8.8.8.8
Write-Host -Separator ""
Write-Host -NoNewline "Temps de reponse moyen en ms : ping 8.8.8.8 : "
$ping = (Test-Connection -ComputerName "8.8.8.8" -Count 4 | Measure-Object -Property ResponseTime -Average)
$ping.Average

