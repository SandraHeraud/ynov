page = """ 
 Qui, d'abord, a l'air d'un roman jadis fait où il s'agissait d'un individu qui dormait tout son saoul

Anton Voyl n'arrivait pas à dormir. Il alluma. Son Jaz marquait minuit vingt. Il poussa un profond soupir, 
s'assit dans son lit, s'appuyant sur son polochon. Il prit un roman, il l'ouvrit, il lut ; mais il n'y saisissait 
qu'un imbroglio confus, il butait à tout instant sur un mot dont il ignorait la signification.

Il abandonna son roman sur son lit. Il alla à son lavabo ; il mouilla un gant qu'il passa sur son front, sur son cou.

Son pouls battait trop fort. Il avait chaud. Il ouvrit son vasistas, scruta la nuit. Il faisait doux. Un bruit 
indistinct montait du faubourg. Un carillon, plus lourd qu'un glas, plus sourd qu'un tocsin, plus profond qu'un 
bourdon, non loin, sonna trois coups. Du canal Saint-Martin, un clapotis plaintif signalait un chaland qui passait.

Sur l'abattant du vasistas, un animal au thorax indigo, à l'aiguillon safran, ni un cafard, ni un charançon, mais 
plutôt un artison, s'avançait, traînant un brin d'alfa. Il s'approcha, voulant l'aplatir d'un coup vif, mais l'animal 
prit son vol, disparaissant dans la nuit avant qu'il ait pu l'assaillir.

Il tapota d'un doigt un air martial sur l'oblong châssis du vasistas.

Il ouvrit son frigo mural, il prit du lait froid, il but un grand bol. Il s'apaisait. Il s'assit sur son cosy, 
il prit un journal qu'il parcourut d'un air distrait. Il alluma un cigarillo qu'il fuma jusqu'au bout quoiqu'il 
trouvât son parfum irritant. Il toussa.

Il mit la radio : un air afro-cubain fut suivi d'un boston, puis un tango, puis un fox-trot, puis un cotillon mis au 
goût du jour. Dutronc chanta du Lanzmann, Barbara un madrigal d'Aragon, Stich-Randall un air d' Aida.

Il dut s'assoupir un instant, car il sursauta soudain. La radio annonçait : « Voici nos Informations ». Il n'y avait 
aucun fait important : à Valparaiso, l'inauguration d'un pont avait fait vingt-cinq morts ; à Zurich, Norodom Sihanouk 
faisait savoir qu'il n'irait pas à Washington ; à Matignon, Pompidou proposait aux syndicats l'organisation d'un statu quo
 social, mais faisait chou blanc. Au Biafra, conflits raciaux ; à Conakry, on parlait d'un putsch. Un typhon s'abattait sur 
 Nagasaki, tandis qu'un ouragan au joli surnom d'Amanda s'annonçait sur Tristan da Cunha dont on rapatriait la population par 
 avions-cargos.

A Roland-Garros, pour finir, dans un match comptant pour la Davis-Cup, Santana avait battu Darmon, six-trois, un-six, trois-six, 
dix-huit, huit-six.

Il coupa la radio. Il s'accroupit sur son tapis, prit son inspiration, fit cinq ou sit tractions, mais il fatigua trop tôt, 
s'assit, fourbu, fixant d'un air las l'intrigant croquis qui apparaissait ou disparaissait sur l'aubusson suivant la façon dont 
s'organisait la vision :

Ainsi, parfois, un rond, pas tout à fait clos, finissant par un trait horizontal : on aurait dit un grand G vu dans un miroir.

Ou, blanc sur blanc, surgissant d'un brouillard cristallin, l'hautain portrait d'un roi brandissant un harpon.

Ou, un court instant, sous trois traits droits, l'apparition d'un croquis approximatif, insatisfaisant : substituts saillants, 
contours bâtards profilant, dans un vain sursaut d'imagination, la Main à trois doigts d'un Sardon ricanant.

Ou, s'imposant soudain, la figuration d'un bourdon au vol lourd, portant sur son thorax noir trois articulations d'un blanc quasi

Son imagination vaquait. Au fur qu'il s'absorbait, scrutant son tapis, il y voyait surgir cinq, six, vingt, vingt-six 
combinaisons, brouillons fascinants mais sans poids, lapsus inconsistants, obscurs portraits qu'il ordonnait sans fin, y traquant 
l'apparition d'un signal plus sûr, d'un signal global dont il aurait aussitôt saisi la signification ; un signal qui l'aurait 
satisfait, alors qu'il voyait, parcours aux maillons incongrus, tout un tas d'imparfaits croquis, dont chacun, aurait-on dit, 
contribuait à ourdir, à bâtir la configuration d'un croquis initial qu'il simulait, qu'il calquait, qu'il approchait mais qu'il 
taisait toujours :

un mort, un voyou, un auto-portrait ;

un bouvillon, un faucon niais, un oisillon couvant son nid ;

un nodus rhumatismal ;

un souhait ;

ou l'iris malin d'un cachalot colossal, narguant Jonas, clouant Caïn, fascinant Achab : avatars d'un noyau vital dont la 
divulgation s'affirmait tabou, substituts ambigus tournant sans fin au tour d'un savoir, d'un pouvoir aboli qui n'apparaîtrait 
plus jamais, mais qu'à jamais, s'abrutissant, il voudrait voir surgir.

Il s'irritait. La vision du tapis lui causait un mal troublant. Sous l'amas d'illusions qu'à tout instant son imagination lui 
dictait, il croyait voir saillir un point nodal, un noyau inconnu qu'il touchait du doigt mais qui toujours lui manquait à 
l'instant où il allait y aboutir.

Il continuait. Il s'obstinait. Fascination dont il n'arrivait pas à s'affranchir. On aurait dit qu'au plus profond du tapis, 
un fil tramait l'obscur point Alpha, miroir du Grand Tout offrant à foison l'Infini du Cosmos, point primordial d'où surgirait 
soudain un panorama total, trou abyssal au rayon nul, champ inconnu dont il traçait l'inouï littoral, dont il suivait l'insinuant 
contour, tourbillon, hauts murs, prison, paroi qu'il parcourait sans jamais la franchir... 
"""

apparitions = {}
for caractere in page:
    # if caractere not in apparitions:
    #     apparitions[caractere] = 1
    # else:
    #     apparitions[caractere] += 1
    apparitions[caractere] = apparitions.get(caractere, 0) + 1

def critere_de_tri(item):
    caractere, nb_apparitions = item
    return nb_apparitions

# print(apparitions)
for caractere, nb_apparitions in sorted(
    apparitions.items(), key=critere_de_tri, reverse=True
)[:10]:
    print(caractere, nb_apparitions)