from pathlib import Path 

message = " j'écris n'importe quoi mais on s'en fout "

with open("../deleteme.txt", "w", encoding="utf-8") as file:
    for char in message:
        ascii_code = ord(char)+3
        ciphered_char = chr(ascii_code)
        file.write(ciphered_char)

with open("../deleteme.txt", "r", encoding="utf-8") as file:
    for line in file:
        for char in line:
            ascii_code = ord(char) - 3
            deciphered_char =chr(ascii_code)
            print(deciphered_char,end="")
print()
